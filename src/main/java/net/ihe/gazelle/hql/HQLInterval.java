package net.ihe.gazelle.hql;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public enum HQLInterval {

	DAY("day", Calendar.DAY_OF_MONTH, 1),

	WEEK("week", Calendar.DAY_OF_MONTH, 7),

	MONTH("month", Calendar.MONTH, 1),

	;

	private String dateTruncParam;
	private int calendarField;
	private int calendarCount;

	HQLInterval(String dateTruncParam, int calendarField, int calendarCount) {
		this.dateTruncParam = dateTruncParam;
		this.calendarField = calendarField;
		this.calendarCount = calendarCount;
	}

	public String getDateTruncParam() {
		return dateTruncParam;
	}

	public Date getEndInterval(Date intervalStartDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(intervalStartDate);
		calendar.add(calendarField, calendarCount);
		return calendar.getTime();
	}

	public void addMissingIntervals(List<HQLIntervalCount>... resultsList) {
		long startTime = Long.MAX_VALUE;
		long endTime = Long.MIN_VALUE;

		boolean doIt = false;

		// first retrieve real start and real end
		for (List<HQLIntervalCount> results : resultsList) {
			if (results.size() > 1) {
				doIt = true;
				Collections.sort(results);
				startTime = Math.min(startTime, results.get(0).getIntervalStartDate().getTime());
				endTime = Math.max(endTime, results.get(results.size() - 1).getIntervalEndDate().getTime());
			}
		}
		// now add missing intervals
		if (doIt) {
			for (List<HQLIntervalCount> results : resultsList) {
				List<HQLIntervalCount> toAdd = new ArrayList<HQLIntervalCount>();

				// start with real start
				Date startDate = new Date(startTime);

				for (HQLIntervalCount hqlIntervalCount : results) {
					// look if interval starts at matching start
					while (Math.abs(hqlIntervalCount.getIntervalStartDate().getTime() - startDate.getTime()) > 100) {
						// add new interval
						Date endDate = getEndInterval(startDate);
						HQLIntervalCount newInterval = new HQLIntervalCount(0, startDate, endDate);
						toAdd.add(newInterval);
						// look for next interval
						startDate = endDate;
					}
					// look for next interval
					startDate = getEndInterval(startDate);
				}
				Date realEndDate = new Date(endTime);

				// check that last interval ends at real end
				while (Math.abs(startDate.getTime() - realEndDate.getTime()) > 100) {
					// add new interval
					Date endDate = getEndInterval(startDate);
					HQLIntervalCount newInterval = new HQLIntervalCount(0, startDate, endDate);
					toAdd.add(newInterval);
					// look for next interval
					startDate = endDate;
				}

				results.addAll(toAdd);
				Collections.sort(results);
			}
		}
	}

}
