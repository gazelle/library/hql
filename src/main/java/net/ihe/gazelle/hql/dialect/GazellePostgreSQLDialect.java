package net.ihe.gazelle.hql.dialect;



import org.hibernate.dialect.PostgreSQLDialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;

public class GazellePostgreSQLDialect extends PostgreSQLDialect {

	public GazellePostgreSQLDialect() {
		super();
		registerFunction("date_part", new StandardSQLFunction("date_part",StandardBasicTypes.DOUBLE));
	}

}
