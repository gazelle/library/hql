package net.ihe.gazelle.hql;

import java.util.Date;

public class HQLIntervalCount implements Comparable<HQLIntervalCount> {

	private int count;
	private Date intervalStartDate;
	private Date intervalEndDate;

	public HQLIntervalCount(int count, Date intervalStartDate, Date intervalEndDate) {
		super();
		this.count = count;
		this.intervalStartDate = intervalStartDate;
		this.intervalEndDate = intervalEndDate;
	}

	public int getCount() {
		return count;
	}

	public Date getIntervalStartDate() {
		return intervalStartDate;
	}

	public Date getIntervalEndDate() {
		return intervalEndDate;
	}

	@Override
	public String toString() {
		return "HQLIntervalCount [count=" + count + ", intervalStartDate=" + intervalStartDate + ", intervalEndDate="
				+ intervalEndDate + "]";
	}

	@Override
	public int compareTo(HQLIntervalCount o) {
		return intervalStartDate.compareTo(o.intervalStartDate);
	}

}
