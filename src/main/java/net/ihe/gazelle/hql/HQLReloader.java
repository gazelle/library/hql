package net.ihe.gazelle.hql;

import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.providers.EntityManagerService;

import org.apache.commons.beanutils.PropertyUtils;

public class HQLReloader {

	private HQLReloader() {
		//
	}

	public static <T> T reloadDetached(T entity) {
		if (entity != null) {
			Object primaryKey = getPrimaryKey(entity);
			if (primaryKey != null) {
				EntityManager entityManager = EntityManagerService.provideEntityManager();
				T attached = (T) entityManager.find(entity.getClass(), primaryKey);
				if (attached != entity) {
					return attached;
				}
			}
		}
		return entity;
	}

	private static Object getPrimaryKey(Object entity) {
		try {
			return PropertyUtils.getProperty(entity, "id");
		} catch (Exception e) {
			throw new RuntimeException("Failed to get id", e);
		}
	}

}
