package net.ihe.gazelle.hql;

public enum HQLPeriod {

	HOUR_OF_DAY("hour"),

	DAY_OF_WEEK("dow"),

	DAY_OF_MONTH("day"),

	DAY_OF_YEAR("doy"),

	WEEK_OF_YEAR("week"),

	MONTH("month");

	String datePartParam = null;

	HQLPeriod(String datePartParam) {
		this.datePartParam = datePartParam;
	}

	public String getDatePartParam() {
		return datePartParam;
	}

}
