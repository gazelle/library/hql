package net.ihe.gazelle.hql;

public interface FilterLabelProvider {

	String getSelectableLabel();

}
