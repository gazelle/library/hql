package net.ihe.gazelle.hql.crud;

import java.util.List;

import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;

public interface HQLForm {

	Class getEntityClass();

	String getId();

	boolean index();

	String getTitle();

	HQLCriterionsForFilter getCriterionsForFilter();

	List<HQLFilter> getFilters();

	List<HQLField> getColumns();

	List<HQLField> getFormFields();

	boolean canCreate();

	Object createNewItem();

	boolean canEdit();

	boolean canEditItem(Object item);

	boolean canDisplay();

	boolean canDisplayItem(Object item);

	boolean canEditField(Object value, HQLField field);

	String getSubFormIdView(HQLField field);

	String getSubFormIdList(HQLField field);

	String getSubFormIdEdit(HQLField field);

	List<?> getPossibleValues(Object value, HQLField field);

}
