package net.ihe.gazelle.hql.crud;

import net.ihe.gazelle.hql.paths.HQLSafePath;

public class HQLField {

	protected HQLSafePath path;
	protected String beanPath;

	protected String label;

	protected boolean sortable;
	protected boolean editable;

	protected HQLFieldType type;

	public HQLField(String label, HQLSafePath path, HQLFieldType type, boolean sortable, boolean editable) {
		super();
		this.path = path;
		this.beanPath = path.toString().replace("this.", "");
		this.label = label;
		this.sortable = sortable;
		this.editable = editable;
		this.type = type;
	}

	public HQLSafePath getPath() {
		return path;
	}

	public String getBeanPath() {
		return beanPath;
	}

	public String getLabel() {
		return label;
	}

	public boolean isSortable() {
		return sortable;
	}

	public boolean isEditable() {
		return editable;
	}

	public HQLFieldType getType() {
		return type;
	}

}
