package net.ihe.gazelle.hql.crud;

public enum HQLFilterType {

	SUGGEST,

	CLASSIC,

	DATE;

}
