package net.ihe.gazelle.hql.crud;

public enum HQLFieldType {

	STRING,

	TEXTAREA,

	HTML,

	DATE,

	DATETIME,

	TOONE_SELECT,

	TOONE_EDIT,

	TOMANY;

}
