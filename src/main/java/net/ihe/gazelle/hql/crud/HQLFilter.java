package net.ihe.gazelle.hql.crud;

public class HQLFilter {

	private HQLFilterType mode;
	private String label;
	private String keyword;

	public HQLFilter(HQLFilterType mode, String label, String keyword) {
		super();
		this.mode = mode;
		this.label = label;
		this.keyword = keyword;
	}

	public HQLFilterType getMode() {
		return mode;
	}

	public String getLabel() {
		return label;
	}

	public String getKeyword() {
		return keyword;
	}

}
