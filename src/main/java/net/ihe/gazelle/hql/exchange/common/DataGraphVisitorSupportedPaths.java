package net.ihe.gazelle.hql.exchange.common;

import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.hql.exchange.visitor.DataGraphVisitor;
import net.ihe.gazelle.hql.paths.HQLSafePath;
import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;

public class DataGraphVisitorSupportedPaths implements DataGraphVisitor {

	private Set<String> supportedPaths;

	public DataGraphVisitorSupportedPaths() {
		super();
		supportedPaths = new TreeSet<String>();
	}

	@Override
	public void acceptEntity(HQLSafePathEntity parent, HQLSafePathEntity path) {
		supportedPaths.add(path.toString());
	}

	@Override
	public void acceptBasic(HQLSafePathEntity parent, HQLSafePathBasic path) {
		supportedPaths.add(path.toString());
	}

	public boolean exports(HQLSafePath attribute) {
		return supportedPaths.contains(attribute.toString());
	}

	@Override
	public void refuseEntity(HQLSafePathEntity parent, HQLSafePathEntity path, String cause) {
		// refused!
	}

}
