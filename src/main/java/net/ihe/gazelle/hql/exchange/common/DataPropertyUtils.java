package net.ihe.gazelle.hql.exchange.common;

import net.ihe.gazelle.hql.paths.HQLSafePath;

import org.apache.commons.beanutils.PropertyUtils;

public class DataPropertyUtils {

	private DataPropertyUtils() {
		super();
	}

	public static String getPropertyName(HQLSafePath<?> hqlSafePath) {
		String path = hqlSafePath.getPath();
		String propertyName = path.substring(path.lastIndexOf('.') + 1);
		return propertyName;
	}

	public static Object evaluate(Object element, HQLSafePath path) throws DataException {
		return evaluate(element, getPropertyName(path));
	}

	public static Object evaluate(Object element, String propertyName) throws DataException {
		Object value;
		try {
			value = PropertyUtils.getProperty(element, propertyName);
		} catch (NoSuchMethodException e) {
			throw new UnknownPropertyException(element.getClass().getCanonicalName() + " - " + propertyName, e);
		} catch (Exception e) {
			throw new DataException("Failed to get property on path " + propertyName, e);
		}
		return value;
	}

}
