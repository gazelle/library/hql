package net.ihe.gazelle.hql.exchange.difference;

import net.ihe.gazelle.hql.exchange.DataImporter;

import org.apache.commons.collections.keyvalue.MultiKey;

public class PropertyDifferent extends ExchangeDifference {

	private MultiKey itemKey;
	private String propertyName;
	private Object dbValue;
	private Object importValue;

	public PropertyDifferent(DataImporter dataImporter, MultiKey itemKey, String propertyName, Object dbValue,
			Object importValue) {
		super(dataImporter);
		this.itemKey = itemKey;
		this.propertyName = propertyName;
		this.dbValue = dbValue;
		this.importValue = importValue;
	}

	public MultiKey getItemKey() {
		return itemKey;
	}

	public String getPropertyName() {
		return propertyName;
	}

	public Object getDbValue() {
		return dbValue;
	}

	public Object getImportValue() {
		return importValue;
	}

	@Override
	public String toString() {
		return "PropertyDifferent [itemKey=" + itemKey + ", propertyName=" + propertyName + ", dbValue=" + dbValue
				+ ", importValue=" + importValue + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemKey == null) ? 0 : itemKey.hashCode());
		result = prime * result + ((propertyName == null) ? 0 : propertyName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PropertyDifferent other = (PropertyDifferent) obj;
		if (itemKey == null) {
			if (other.itemKey != null)
				return false;
		} else if (!itemKey.equals(other.itemKey))
			return false;
		if (propertyName == null) {
			if (other.propertyName != null)
				return false;
		} else if (!propertyName.equals(other.propertyName))
			return false;
		return true;
	}

}
