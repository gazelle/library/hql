package net.ihe.gazelle.hql.exchange;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;

import net.ihe.gazelle.hql.HQLQueryBuilderInterface;
import net.ihe.gazelle.hql.exchange.common.DataException;
import net.ihe.gazelle.hql.exchange.common.DataExchangeTools;
import net.ihe.gazelle.hql.exchange.common.DataFormatter;
import net.ihe.gazelle.hql.exchange.common.DataGraphVisitorSupportedPaths;
import net.ihe.gazelle.hql.exchange.common.DataPropertyUtils;
import net.ihe.gazelle.hql.exchange.difference.ExchangeDifference;
import net.ihe.gazelle.hql.exchange.difference.ItemDoesNotExist;
import net.ihe.gazelle.hql.exchange.difference.PropertyDifferent;
import net.ihe.gazelle.hql.paths.HQLEntityInfo;
import net.ihe.gazelle.hql.paths.HQLSafePath;
import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.providers.detached.HibernateActionPerformer;
import net.ihe.gazelle.hql.providers.detached.HibernateFailure;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.keyvalue.MultiKey;

public class DataImporter {

	private Map<MultiKey, Map<String, Object>> dictionnary;

	private Set<MultiKey> dataItems;

	private Class<? extends ExchangeSet> set;

	public DataImporter(DataFormatter dataFormatter, InputStream os) throws DataException {
		super();
		this.dictionnary = new HashMap<MultiKey, Map<String, Object>>();
		this.dataItems = new HashSet<MultiKey>();

		dataFormatter.readStart(os);

		String setClassName = dataFormatter.readSetClassName();
		try {
			this.set = (Class<? extends ExchangeSet>) Class.forName(setClassName);
		} catch (ClassNotFoundException e) {
			throw new DataException("Failed to find " + setClassName, e);
		}

		Map<String, Object> readMap = dataFormatter.readMap();
		while (readMap != null) {
			MultiKey key = (MultiKey) readMap.get("key_");
			readMap.remove("key_");
			dictionnary.put(key, readMap);
			dataItems.add(key);
			readMap = dataFormatter.readMap();
		}
		dataFormatter.readEnd();
	}

	public Set<ExchangeDifference> getAllDifferences() throws DataException {

		Set<ExchangeDifference> allDifferences = new TreeSet<ExchangeDifference>();

		// allDifferences.addAll(getDifferences(new MultiKey("net.ihe.gazelle.tf.model.Document", 50)));

		for (MultiKey multiKey : dictionnary.keySet()) {
			allDifferences.addAll(getDifferences(multiKey));
		}

		return allDifferences;
	}

	public void patchDifferences(Set<ExchangeDifference> allDifferences) throws DataException {
		try {
			Map<MultiKey, Set<MultiKey>> references = getReferences(allDifferences);

			Set<MultiKey> result = (Set<MultiKey>) HibernateActionPerformer.performHibernateAction(
					new DataImporterCreateInstances(), references, dictionnary);

			for (MultiKey multiKey : result) {
				HibernateActionPerformer.performHibernateAction(new DataImporterCreateCollections(), multiKey,
						dictionnary);
			}

			for (ExchangeDifference exchangeDifference : allDifferences) {
				if (exchangeDifference instanceof PropertyDifferent) {
					PropertyDifferent propertyDifferent = (PropertyDifferent) exchangeDifference;
					System.out.println(propertyDifferent);
					HibernateActionPerformer.performHibernateAction(new DataImporterSetProperty(), propertyDifferent,
							dictionnary);
				}
			}

		} catch (HibernateFailure e) {
			throw new DataException(e);
		}
	}

	protected Set<ExchangeDifference> getDifferences(MultiKey itemKey) throws DataException {
		Set<ExchangeDifference> result = new HashSet<ExchangeDifference>();
		HQLSafePathEntity<?> hqlEntity = DataExchangeTools.getHQLEntityByMultikey(itemKey);

		// Retrieve paths to visit
		DataGraphVisitorSupportedPaths dataGraph = new DataGraphVisitorSupportedPaths();
		HQLEntityInfo.visitDataGraph(hqlEntity, dataGraph, set);

		compareEntitySingle(hqlEntity, dataGraph, itemKey, result);
		return result;
	}

	private void compareEntitySingle(HQLSafePathEntity<?> hqlEntity, DataGraphVisitorSupportedPaths dataGraph,
			MultiKey itemKey, Set<ExchangeDifference> result) throws DataException {
		Map<String, Object> importValues = dictionnary.get(itemKey);
		if (importValues == null) {
			throw new DataException("Missing " + itemKey + " in dictionnary!");
		} else {
			Object dbElement = findElementByUniqueAttributes(itemKey, dictionnary);
			if (dbElement == null) {
				result.add(new ItemDoesNotExist(this, itemKey));
			} else {
				compare(hqlEntity, dataGraph, itemKey, dbElement, result);
			}
		}
	}

	private void compare(HQLSafePathEntity<?> hqlEntity, DataGraphVisitorSupportedPaths dataGraph, MultiKey itemKey,
			Object dbElement, Set<ExchangeDifference> result) throws DataException {

		Map<String, Object> importValues = dictionnary.get(itemKey);

		Map<String, HQLSafePath> properties = new HashMap<String, HQLSafePath>();

		// process all attributes except id attributes and mapped by attributes
		Set<HQLSafePath<?>> allAttributes = HQLEntityInfo.getAllAttributes(hqlEntity);
		allAttributes.removeAll(HQLEntityInfo.getIdAttributes(hqlEntity));
		allAttributes.removeAll(HQLEntityInfo.getMappedByAttributes(hqlEntity));

		for (HQLSafePath<?> hqlSafePath : allAttributes) {
			properties.put(DataPropertyUtils.getPropertyName(hqlSafePath), hqlSafePath);
		}

		Set<Entry<String, Object>> entrySet = importValues.entrySet();
		for (Entry<String, Object> entry : entrySet) {
			String propertyName = entry.getKey();
			HQLSafePath property = properties.get(propertyName);

			if (property != null) {
				Object importValue = entry.getValue();
				Object dbValue = DataPropertyUtils.evaluate(dbElement, propertyName);

				// WARN : dbValue and importValue may be null

				if (property instanceof HQLSafePathBasic) {
					compareBasic(itemKey, propertyName, dbValue, importValue, result);
				} else if (property instanceof HQLSafePathEntity && dataGraph.exports(property)) {
					HQLSafePathEntity<?> hqlSafePathEntity = (HQLSafePathEntity<?>) property;
					if (hqlSafePathEntity.isSingle()) {

						boolean equals;

						if (importValue == null && dbValue == null) {
							equals = true;
						} else if (importValue == null || dbValue == null) {
							equals = false;
						} else {
							MultiKey importUniqueAttributesValue = DataExchangeTools
									.getUniqueAttributesFromDictionnary((MultiKey) importValue, dictionnary);
							MultiKey dbUniqueAttributesValue = DataExchangeTools.getUniqueAttributesFromDB(dbValue);
							equals = importUniqueAttributesValue.equals(dbUniqueAttributesValue);
						}

						if (!equals) {
							result.add(new PropertyDifferent(this, itemKey, propertyName, dbValue, importValue));
						}
					} else {

						Collection<MultiKey> importCollection = (Collection<MultiKey>) importValue;
						Collection<Object> dbCollection = (Collection<Object>) dbValue;

						compareCollection(hqlSafePathEntity, itemKey, dbCollection, importCollection, result,
								propertyName);

					}
				}
			}
		}
	}

	private void compareBasic(MultiKey itemKey, String propertyName, Object dbValue, Object importValue,
			Set<ExchangeDifference> result) throws DataException {
		if (!propertyEquals(dbValue, importValue)) {
			result.add(new PropertyDifferent(this, itemKey, propertyName, dbValue, importValue));
		}
	}

	private boolean propertyEquals(Object value1, Object value2) {
		if (value1 == null && value2 != null) {
			return false;
		}
		if (value1 != null && value2 == null) {
			return false;
		}
		if (value1 == null && value2 == null) {
			return true;
		}
		return value1.equals(value2);
	}

	private void compareCollection(HQLSafePathEntity hqlSafePathEntity, MultiKey itemKey, Collection<Object> dbValue,
			Collection<MultiKey> importValue, Set<ExchangeDifference> result, String propertyName) throws DataException {

		// same type, same length, not null
		boolean equals = true;

		if (!collectionEqualsBasic(dbValue, importValue)) {
			equals = false;
		} else {
			Set<MultiKey> importUniqueAttributesSet = new HashSet<MultiKey>();
			for (MultiKey multiKey : importValue) {
				importUniqueAttributesSet.add(DataExchangeTools.getUniqueAttributesFromDictionnary(multiKey,
						dictionnary));
			}

			for (Object dbEntry : dbValue) {

				MultiKey dbUniqueAttributesValue = DataExchangeTools.getUniqueAttributesFromDB(dbEntry);
				if (!importUniqueAttributesSet.remove(dbUniqueAttributesValue)) {
					equals = false;
					break;
				}

			}

			if (importUniqueAttributesSet.size() != 0) {
				// should not happen in a perfect world
				equals = false;
			}
		}

		if (!equals) {
			result.add(new PropertyDifferent(this, itemKey, propertyName, dbValue, importValue));
		}
	}

	private boolean collectionEqualsBasic(Collection<Object> dbValue, Collection<MultiKey> importValue) {
		if (dbValue == null && importValue != null) {
			return false;
		}
		if (dbValue != null && importValue == null) {
			return false;
		}
		if (dbValue == null && importValue == null) {
			return true;
		}

		if (dbValue.size() != importValue.size()) {
			return false;
		}

		return true;
	}

	protected Map<MultiKey, Set<MultiKey>> getReferences(Set<ExchangeDifference> allDifferences) throws DataException {
		Map<MultiKey, Set<MultiKey>> references = new HashMap<MultiKey, Set<MultiKey>>();
		Set<MultiKey> missingEntities = references.keySet();

		for (ExchangeDifference exchangeDifference : allDifferences) {
			if (exchangeDifference instanceof ItemDoesNotExist) {
				MultiKey itemKey = ((ItemDoesNotExist) exchangeDifference).getItemKey();
				Set<MultiKey> referencing = new HashSet<MultiKey>();
				references.put(itemKey, referencing);
			}
		}

		for (ExchangeDifference exchangeDifference : allDifferences) {
			if (exchangeDifference instanceof ItemDoesNotExist) {
				MultiKey itemKey = ((ItemDoesNotExist) exchangeDifference).getItemKey();

				HQLSafePathEntity<?> hqlEntity = DataExchangeTools.getHQLEntityByMultikey(itemKey);

				Set<HQLSafePath<?>> notNullAttributesSet = HQLEntityInfo.getNotNullAttributes(hqlEntity);
				notNullAttributesSet.addAll(HQLEntityInfo.getIdAttributes(hqlEntity));
				notNullAttributesSet.addAll(HQLEntityInfo.getUniqueAttributesSet(hqlEntity));

				notNullAttributesSet.removeAll(HQLEntityInfo.getMappedByAttributes(hqlEntity));

				Set<String> notNullproperties = new HashSet<String>();
				for (HQLSafePath<?> hqlSafePath : notNullAttributesSet) {
					notNullproperties.add(DataPropertyUtils.getPropertyName(hqlSafePath));
				}

				Set<MultiKey> referencing = references.get(itemKey);

				Map<String, Object> values = dictionnary.get(itemKey);
				for (Entry<String, Object> entry : values.entrySet()) {
					if (notNullproperties.contains(entry.getKey())) {
						if (entry.getValue() instanceof MultiKey) {
							if (missingEntities.contains((MultiKey) entry.getValue())) {
								referencing.add((MultiKey) entry.getValue());
							}
						}
					}
				}

			}
		}
		return references;
	}

	public static Collection convertCollection(Collection importValue, Map<MultiKey, Map<String, Object>> dictionnary)
			throws DataException {
		Collection result;
		if (importValue instanceof List) {
			result = new ArrayList(importValue.size());
		} else {
			result = new HashSet(importValue.size());
		}

		for (Object object : importValue) {
			Object o = null;
			if (object instanceof MultiKey) {
				o = findElementByUniqueAttributes((MultiKey) object, dictionnary);
			} else {
				o = object;
			}
			result.add(o);
		}
		return result;
	}

	public static void convertAndSetProperty(Object o, String propertyName, Object importValue,
			Map<MultiKey, Map<String, Object>> dictionnary) throws DataException {
		if (importValue instanceof Collection) {
			importValue = convertCollection((Collection) importValue, dictionnary);
		}

		if (importValue instanceof MultiKey) {
			importValue = findElementByUniqueAttributes((MultiKey) importValue, dictionnary);
		}
		try {
			PropertyUtils.setProperty(o, propertyName, importValue);
		} catch (Exception e) {
			throw new DataException(e);
		}

		EntityManagerService.provideEntityManager().merge(o);
	}

	public static Object findElementByUniqueAttributes(MultiKey dataItem, Map<MultiKey, Map<String, Object>> dictionnary)
			throws DataException {
		HQLSafePathEntity<?> hqlEntity = DataExchangeTools.getHQLEntityByMultikey(dataItem);
		HQLQueryBuilderInterface queryBuilderInterface = (HQLQueryBuilderInterface) hqlEntity;
		Set<HQLSafePath<?>> attributePaths = HQLEntityInfo.getUniqueAttributesSetWithoutIds(hqlEntity);

		Map<String, Object> values = dictionnary.get(dataItem);
		Map<String, Object> constraints = new HashMap<String, Object>();

		for (HQLSafePath<?> hqlSafePath : attributePaths) {
			String propertyName = DataPropertyUtils.getPropertyName(hqlSafePath);
			Object value = values.get(propertyName);
			constraints.put(hqlSafePath.toString(), value);
		}

		for (Entry<String, Object> constraint : constraints.entrySet()) {
			if (constraint.getValue() instanceof MultiKey) {
				Object value = findElementByUniqueAttributes((MultiKey) constraint.getValue(), dictionnary);
				queryBuilderInterface.addRestriction(HQLRestrictions.eq(constraint.getKey(), value));
			} else {
				queryBuilderInterface.addRestriction(HQLRestrictions.eq(constraint.getKey(), constraint.getValue()));
			}
		}

		if (queryBuilderInterface.getCount() > 1) {
			throw new DataException("\nFound multiple instances when looking for " + dataItem
					+ "\nShould be unique for " + constraints);
		}
		Object element = queryBuilderInterface.getUniqueResult();
		return element;
	}

}
