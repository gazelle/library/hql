package net.ihe.gazelle.hql.exchange.common;

public class UnknownPropertyException extends DataException {

	private static final long serialVersionUID = 523651320373592472L;

	public UnknownPropertyException() {
		super();
	}

	public UnknownPropertyException(String message, Throwable cause) {
		super(message, cause);
	}

	public UnknownPropertyException(String message) {
		super(message);
	}

	public UnknownPropertyException(Throwable cause) {
		super(cause);
	}

}
