package net.ihe.gazelle.hql.exchange;

import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import net.ihe.gazelle.hql.HQLQueryBuilderInterface;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.exchange.common.DataException;
import net.ihe.gazelle.hql.exchange.common.DataExchangeTools;
import net.ihe.gazelle.hql.exchange.common.DataFormatter;
import net.ihe.gazelle.hql.exchange.common.DataGraphVisitorSupportedPaths;
import net.ihe.gazelle.hql.exchange.common.DataPropertyUtils;
import net.ihe.gazelle.hql.paths.HQLEntityInfo;
import net.ihe.gazelle.hql.paths.HQLSafePath;
import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DataExporter {

	private static Logger log = LoggerFactory.getLogger(DataExporter.class);

	private Map<MultiKey, Map<String, Object>> dictionnary;

	private Set<MultiKey> dataItems;

	private Class<? extends ExchangeSet> set;

	public DataExporter(Class<? extends ExchangeSet> set) throws DataException {
		super();
		this.dictionnary = new HashMap<MultiKey, Map<String, Object>>();
		this.dataItems = new HashSet<MultiKey>();
		this.set = set;
	}

	public boolean isCompatibleSet(HQLSafePathEntity<?> hqlSafePathEntity) {
		Class<?> entityClass = hqlSafePathEntity.getEntityClass();
		Exchanged dbSynchronized = entityClass.getAnnotation(Exchanged.class);

		if (dbSynchronized == null || !dbSynchronized.set().isAssignableFrom(set)) {
			return false;
		} else {
			return true;
		}
	}

	// Add an element to the export
	public void writeElement(Object element) throws DataException {
		// Retrieve paths to visit
		HQLSafePathEntity<?> hqlEntity = DataExchangeTools.getHQLEntity(element);
		DataGraphVisitorSupportedPaths dataGraph = new DataGraphVisitorSupportedPaths();
		HQLEntityInfo.visitDataGraph(hqlEntity, dataGraph, set);

		// collect all instances of the tree
		collectItems(dataGraph, hqlEntity, element);
	}

	private MultiKey collectItems(DataGraphVisitorSupportedPaths dataGraph, HQLSafePathEntity<?> hqlEntity,
			Object element) throws DataException {

		MultiKey id = DataExchangeTools.getIdsFromDB(element);
		// not already processed
		if (dataItems.add(id)) {

			Set<HQLSafePath<?>> allAttributes = HQLEntityInfo.getAllAttributes(hqlEntity);

			for (HQLSafePath<?> hqlSafePath : allAttributes) {
				if (hqlSafePath instanceof HQLSafePathEntity && dataGraph.exports(hqlSafePath)) {
					HQLSafePathEntity<?> hqlSafePathEntity = (HQLSafePathEntity<?>) hqlSafePath;
					if (isCompatibleSet(hqlSafePathEntity)) {
						Object value = DataPropertyUtils.evaluate(element, hqlSafePath);
						if (value != null) {
							if (hqlSafePathEntity.isSingle()) {
								collectItems(dataGraph, hqlSafePathEntity, value);
							} else {
								Collection<Object> objects = (Collection<Object>) value;
								for (Object object : objects) {
									if (object != null) {
										collectItems(dataGraph, hqlSafePathEntity, object);
									}
								}
							}
						}
					}
				}
			}
		}

		return id;
	}

	// Write the export
	public void write(DataFormatter dataFormatter, OutputStream os) throws DataException {
		// export all instances to dictionnary
		for (MultiKey dataItem : dataItems) {
			addItemToDictionnary(dataItem);
		}

		dataFormatter.writeStart(os);
		dataFormatter.writeSetClassName(set.getCanonicalName());
		Set<Entry<MultiKey, Map<String, Object>>> entrySet = dictionnary.entrySet();
		for (Entry<MultiKey, Map<String, Object>> entry : entrySet) {
			Map<String, Object> itemProperties = new HashMap<String, Object>();
			itemProperties.putAll(entry.getValue());
			itemProperties.put("key_", entry.getKey());
			dataFormatter.writeMap(itemProperties);
		}
		dataFormatter.writeEnd();
	}

	private void addItemToDictionnary(MultiKey dataItem) throws DataException {
		Map<String, Object> dictionnaryEntry = new HashMap<String, Object>();

		Object element = findElement(dataItem);

		HQLSafePathEntity<?> hqlEntity = DataExchangeTools.getHQLEntityByMultikey(dataItem);
		Set<HQLSafePath<?>> allAttributes = HQLEntityInfo.getAllAttributes(hqlEntity);

		// test entity uniqueness columns without ids
		HQLEntityInfo.getUniqueAttributesSetWithoutIds(hqlEntity);

		// may we trace all attributes that appear in the paths, perf improver
		// and use only these ones instead of browsing all of them looking for
		// exported elements
		for (HQLSafePath<?> hqlSafePath : allAttributes) {
			addItemAttributeToDictionnary(dictionnaryEntry, element, hqlSafePath);
		}

		dictionnary.put(dataItem, dictionnaryEntry);
	}

	public static Object findElement(MultiKey dataItem) throws DataException {
		HQLSafePathEntity<?> hqlEntity = DataExchangeTools.getHQLEntityByMultikey(dataItem);
		HQLQueryBuilderInterface queryBuilderInterface = (HQLQueryBuilderInterface) hqlEntity;
		Set<HQLSafePath<?>> idAttributes = HQLEntityInfo.getIdAttributes(hqlEntity);
		int i = 1;
		for (HQLSafePath<?> hqlSafePath : idAttributes) {
			Object value = null;
			if (hqlSafePath instanceof HQLSafePathBasic) {
				value = dataItem.getKey(i);
			} else {
				MultiKey subKey = (MultiKey) dataItem.getKey(i);
				value = findElement(subKey);
			}
			queryBuilderInterface.addRestriction(HQLRestrictions.eq(hqlSafePath.toString(), value));
			i++;
		}
		Object element = queryBuilderInterface.getUniqueResult();
		return element;
	}

	private void addItemAttributeToDictionnary(Map<String, Object> dictionnaryEntry, Object element,
			HQLSafePath<?> hqlSafePath) throws DataException {
		String propertyName = DataPropertyUtils.getPropertyName(hqlSafePath);

		if (hqlSafePath instanceof HQLSafePathBasic) {
			Object value = DataPropertyUtils.evaluate(element, propertyName);
			dictionnaryEntry.put(propertyName, value);
		} else if (hqlSafePath instanceof HQLSafePathEntity) {
			HQLSafePathEntity<?> hqlSafePathEntity = (HQLSafePathEntity<?>) hqlSafePath;
			if (isCompatibleSet(hqlSafePathEntity)) {
				if (hqlSafePathEntity.isSingle()) {
					addItemSingleAttributeToDictionnary(dictionnaryEntry, element, propertyName);
				} else {
					addItemMultipleAttributeToDictionnary(dictionnaryEntry, element, hqlSafePath, propertyName);
				}
			}
		}

	}

	private void addItemSingleAttributeToDictionnary(Map<String, Object> dictionnaryEntry, Object element,
			String propertyName) throws DataException {
		Object object = DataPropertyUtils.evaluate(element, propertyName);
		if (object == null) {
			dictionnaryEntry.put(propertyName, null);
		} else {
			MultiKey id = DataExchangeTools.getIdsFromDB(object);
			if (dataItems.contains(id)) {
				dictionnaryEntry.put(propertyName, id);
			}
		}
	}

	private void addItemMultipleAttributeToDictionnary(Map<String, Object> dictionnaryEntry, Object element,
			HQLSafePath<?> hqlSafePath, String propertyName) throws DataException {
		Collection<MultiKey> children;

		Collection<Object> objects = (Collection<Object>) DataPropertyUtils.evaluate(element, propertyName);
		boolean allIn = true;
		if (objects == null) {
			children = null;
		} else {
			if (objects instanceof List) {
				children = new ArrayList<MultiKey>();
			} else {
				children = new HashSet<MultiKey>();
			}
			allIn = true;

			for (Object object : objects) {
				MultiKey childValue = null;
				if (object != null) {
					childValue = DataExchangeTools.getIdsFromDB(object);
					if (!dataItems.contains(childValue)) {
						allIn = false;
						break;
					}
				}
				children.add(childValue);
			}
		}

		if (allIn) {
			dictionnaryEntry.put(propertyName, children);
		}
	}

}
