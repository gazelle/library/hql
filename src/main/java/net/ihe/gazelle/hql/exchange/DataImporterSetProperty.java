package net.ihe.gazelle.hql.exchange;

import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.collections.keyvalue.MultiKey;

import net.ihe.gazelle.hql.exchange.difference.PropertyDifferent;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;

public class DataImporterSetProperty implements PerformHibernateAction {

	@Override
	public Object performAction(EntityManager entityManager, Object... context) throws Exception {
		PropertyDifferent propertyDifferent = (PropertyDifferent) context[0];
		Map<MultiKey, Map<String, Object>> dictionnary = (Map<MultiKey, Map<String, Object>>) context[1];

		MultiKey itemKey = propertyDifferent.getItemKey();

		Object o = DataImporter.findElementByUniqueAttributes(itemKey, dictionnary);
		String propertyName = propertyDifferent.getPropertyName();
		Object importValue = propertyDifferent.getImportValue();

		DataImporter.convertAndSetProperty(o, propertyName, importValue, dictionnary);

		return null;
	}

}
