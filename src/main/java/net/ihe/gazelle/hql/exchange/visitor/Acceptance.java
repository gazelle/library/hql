package net.ihe.gazelle.hql.exchange.visitor;

public class Acceptance {

	private boolean accepted;

	private String cause;

	public Acceptance(boolean accepted, String cause) {
		super();
		this.accepted = accepted;
		this.cause = cause;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public String getCause() {
		return cause;
	}

}
