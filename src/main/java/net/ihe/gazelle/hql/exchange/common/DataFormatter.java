package net.ihe.gazelle.hql.exchange.common;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

public interface DataFormatter {

	void readStart(InputStream inputStream) throws DataException;

	String readSetClassName() throws DataException;

	/**
	 * @return new map, if null if no more
	 * @throws DataException
	 */
	Map<String, Object> readMap() throws DataException;

	void readEnd() throws DataException;

	void writeStart(OutputStream outputStream) throws DataException;

	void writeSetClassName(String setClassName) throws DataException;

	void writeMap(Map<String, Object> map) throws DataException;

	void writeEnd() throws DataException;

}
