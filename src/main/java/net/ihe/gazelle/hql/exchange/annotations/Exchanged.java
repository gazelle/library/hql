/**
 * 
 */
package net.ihe.gazelle.hql.exchange.annotations;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import net.ihe.gazelle.hql.exchange.ExchangeSet;

/**
 * Added to classes synchronized using Slony between GMM and TM for instance
 * 
 * @author glandais
 * 
 */
@Target({ TYPE })
@Retention(RUNTIME)
public @interface Exchanged {

	Class<? extends ExchangeSet> set();

}
