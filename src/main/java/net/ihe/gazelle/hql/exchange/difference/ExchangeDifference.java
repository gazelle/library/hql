package net.ihe.gazelle.hql.exchange.difference;

import net.ihe.gazelle.hql.exchange.DataImporter;

public abstract class ExchangeDifference implements Comparable<ExchangeDifference> {

	protected DataImporter dataImporter;

	public ExchangeDifference(DataImporter dataImporter) {
		super();
		this.dataImporter = dataImporter;
	}

	public DataImporter getDataImporter() {
		return dataImporter;
	}

	@Override
	public int compareTo(ExchangeDifference o) {
		return toString().compareTo(o.toString());
	}
}
