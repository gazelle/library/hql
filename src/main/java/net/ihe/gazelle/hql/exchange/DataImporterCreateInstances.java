package net.ihe.gazelle.hql.exchange;

import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.persistence.EntityManager;

import net.ihe.gazelle.hql.exchange.common.DataException;
import net.ihe.gazelle.hql.exchange.common.DataExchangeTools;
import net.ihe.gazelle.hql.exchange.common.DataPropertyUtils;
import net.ihe.gazelle.hql.paths.HQLEntityInfo;
import net.ihe.gazelle.hql.paths.HQLSafePath;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.keyvalue.MultiKey;

public class DataImporterCreateInstances implements PerformHibernateAction {

	private Map<MultiKey, Map<String, Object>> dictionnary;

	@Override
	public Object performAction(EntityManager entityManager, Object... context) throws Exception {
		Map<MultiKey, Set<MultiKey>> references = (Map<MultiKey, Set<MultiKey>>) context[0];
		this.dictionnary = (Map<MultiKey, Map<String, Object>>) context[1];

		Set<MultiKey> result = new HashSet<MultiKey>();

		int lastCount = -1;

		while (references.size() != lastCount) {
			lastCount = references.size();

			System.out.println("******************");
			System.out.println(references.size() + " objects to create");

			Set<MultiKey> toBeCreated = new HashSet<MultiKey>();
			for (Entry<MultiKey, Set<MultiKey>> entry : references.entrySet()) {
				if (entry.getValue().size() == 0) {
					toBeCreated.add(entry.getKey());
				}
			}

			for (MultiKey multiKey : toBeCreated) {
				// create multiKey
				System.out.println("Creating " + multiKey);

				Object instance = createInstanceWithoutCollections(multiKey);

				result.add(multiKey);

				references.remove(multiKey);
			}

			EntityManagerService.provideEntityManager().flush();

			for (Entry<MultiKey, Set<MultiKey>> entry : references.entrySet()) {
				entry.getValue().removeAll(toBeCreated);
			}
		}

		if (references.size() != 0) {
			for (Entry<MultiKey, Set<MultiKey>> reference : references.entrySet()) {
				System.out.println("***************");
				System.out.println(reference.getKey());
				Set<MultiKey> values = reference.getValue();
				for (MultiKey multiKey : values) {
					System.out.println(" -> " + multiKey);
				}
			}
			System.out.println("***************");
			throw new DataException("Failed to create instances in database... (references loop)");
		}

		return result;
	}

	/**
	 * Recreate a complete instance from dictionnary, without collections!
	 * 
	 * @param itemKey
	 * @return
	 * @throws DataException
	 */
	private Object createInstanceWithoutCollections(MultiKey itemKey) throws DataException {
		String className = (String) itemKey.getKey(0);
		Object result;
		try {
			result = Class.forName(className).newInstance();
		} catch (Exception e) {
			throw new DataException(e);
		}

		HQLSafePathEntity<?> hqlEntity = DataExchangeTools.getHQLEntityByMultikey(itemKey);

		Set<HQLSafePath<?>> idAttributes = HQLEntityInfo.getIdAttributes(hqlEntity);

		Set<String> idProperties = new HashSet<String>();
		for (HQLSafePath<?> hqlSafePath : idAttributes) {
			idProperties.add(DataPropertyUtils.getPropertyName(hqlSafePath));
		}

		Set<Entry<String, Object>> entrySet = dictionnary.get(itemKey).entrySet();
		for (Entry<String, Object> entry : entrySet) {
			Object value = entry.getValue();
			if (!(value instanceof Collection) && !idProperties.contains(entry.getKey())) {
				if (value instanceof MultiKey) {
					value = DataImporter.findElementByUniqueAttributes((MultiKey) value, dictionnary);
				}
				try {
					PropertyUtils.setProperty(result, entry.getKey(), value);
				} catch (Exception e) {
					throw new DataException(e);
				}
			}
		}

		EntityManagerService.provideEntityManager().persist(result);

		return result;
	}

}
