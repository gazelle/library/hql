package net.ihe.gazelle.hql.exchange.formatters;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import com.thoughtworks.xstream.XStream;

public class XmlFormatter extends ObjectAbstractFormatter {

	private XStream xstream;

	public XmlFormatter() {
		super();
		xstream = new XStream();
	}

	@Override
	protected ObjectInputStream getObjectInputStream(InputStream inputStream) throws IOException {
		return xstream.createObjectInputStream(inputStream);
	}

	@Override
	protected ObjectOutputStream getObjectOutputStream(OutputStream outputStream) throws IOException {
		return xstream.createObjectOutputStream(outputStream);
	}

}
