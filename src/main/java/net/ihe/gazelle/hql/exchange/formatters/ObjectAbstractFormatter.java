package net.ihe.gazelle.hql.exchange.formatters;

import net.ihe.gazelle.hql.exchange.common.DataException;
import net.ihe.gazelle.hql.exchange.common.DataFormatter;

import java.io.*;
import java.util.Map;

public abstract class ObjectAbstractFormatter implements DataFormatter {

	private ObjectInputStream objectInputStream;
	private ObjectOutputStream objectOutputStream;

	protected abstract ObjectInputStream getObjectInputStream(InputStream inputStream) throws IOException;

	protected abstract ObjectOutputStream getObjectOutputStream(OutputStream outputStream) throws IOException;

	@Override
	public void readStart(InputStream inputStream) throws DataException {
		try {
			objectInputStream = getObjectInputStream(inputStream);
			int readVersion = (Integer) readObject();
		} catch (IOException e) {
			throw new DataException(e);
		}
	}

	private Object readObject() throws DataException {
		try {
			return objectInputStream.readObject();
		} catch (IOException e) {
			throw new DataException(e);
		} catch (ClassNotFoundException e) {
			throw new DataException(e);
		}
	}

	@Override
	public String readSetClassName() throws DataException {
		return (String) readObject();
	}

	@Override
	public Map<String, Object> readMap() throws DataException {
		Object next = readObject();
		return (Map<String, Object>) next;
	}

	@Override
	public void readEnd() throws DataException {
		try {
			objectInputStream.close();
		} catch (IOException e) {
			throw new DataException(e);
		}
	}

	@Override
	public void writeStart(OutputStream outputStream) throws DataException {
		try {
			objectOutputStream = getObjectOutputStream(outputStream);
			writeObject(Integer.valueOf(1));
		} catch (IOException e) {
			throw new DataException(e);
		}
	}

	public void writeObject(Object o) throws DataException {
		try {
			objectOutputStream.writeObject(o);
		} catch (IOException e) {
			throw new DataException(e);
		}
	}

	@Override
	public void writeSetClassName(String setClassName) throws DataException {
		writeObject(setClassName);
	}

	@Override
	public void writeMap(Map<String, Object> map) throws DataException {
		writeObject(map);
	}

	@Override
	public void writeEnd() throws DataException {
		writeObject(null);
		try {
			objectOutputStream.close();
		} catch (IOException e) {
			throw new DataException(e);
		}
	}

}
