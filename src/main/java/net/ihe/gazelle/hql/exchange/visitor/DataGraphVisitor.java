package net.ihe.gazelle.hql.exchange.visitor;

import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;

public interface DataGraphVisitor {

	void acceptEntity(HQLSafePathEntity parent, HQLSafePathEntity path);

	void refuseEntity(HQLSafePathEntity parent, HQLSafePathEntity path, String cause);

	void acceptBasic(HQLSafePathEntity parent, HQLSafePathBasic path);

}
