package net.ihe.gazelle.hql.exchange.formatters;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class BinaryFormatter extends ObjectAbstractFormatter {

	@Override
	protected ObjectInputStream getObjectInputStream(InputStream inputStream) throws IOException {
		return new ObjectInputStream(new GZIPInputStream(inputStream));
	}

	@Override
	protected ObjectOutputStream getObjectOutputStream(OutputStream outputStream) throws IOException {
		return new ObjectOutputStream(new GZIPOutputStream(outputStream));
	}

}
