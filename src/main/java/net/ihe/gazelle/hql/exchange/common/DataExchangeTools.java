package net.ihe.gazelle.hql.exchange.common;

import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.hql.exchange.DataExporter;
import net.ihe.gazelle.hql.paths.HQLEntityInfo;
import net.ihe.gazelle.hql.paths.HQLSafePath;
import net.ihe.gazelle.hql.paths.HQLSafePathBasic;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;

import org.apache.commons.collections.keyvalue.MultiKey;

public class DataExchangeTools {

	protected static MultiKey getFromDB(boolean uniqueAttributes, HQLSafePathEntity<?> hqlEntity, Object dbEntry)
			throws DataException {
		Set<HQLSafePath<?>> attributePaths;
		if (uniqueAttributes) {
			attributePaths = HQLEntityInfo.getUniqueAttributesSetWithoutIds(hqlEntity);
		} else {
			attributePaths = HQLEntityInfo.getIdAttributes(hqlEntity);
		}

		Object[] attributes = new Object[attributePaths.size() + 1];
		attributes[0] = hqlEntity.getEntityClass().getCanonicalName();

		int i = 1;
		for (HQLSafePath<?> hqlSafePath : attributePaths) {
			Object value = null;
			value = DataPropertyUtils.evaluate(dbEntry, hqlSafePath);
			if (hqlSafePath instanceof HQLSafePathBasic) {
				attributes[i] = value;
			} else {
				HQLSafePathEntity hqlSafePathEntity = (HQLSafePathEntity) hqlSafePath;
				if (!hqlSafePathEntity.isSingle()) {
					throw new DataException(hqlSafePathEntity + " not single!?");
				}
				if (value == null) {
					attributes[i] = null;
				} else {
					attributes[i] = getFromDB(uniqueAttributes, hqlSafePathEntity, value);
				}
			}
			i++;
		}

		return new MultiKey(attributes);
	}

	public static MultiKey getIdsFromDB(Object element) throws DataException {
		HQLSafePathEntity<?> hqlEntity = getHQLEntity(element);
		return getFromDB(false, hqlEntity, element);
	}

	public static MultiKey getUniqueAttributesFromDB(Object dbEntry) throws DataException {
		HQLSafePathEntity<?> hqlEntity = getHQLEntity(dbEntry);
		return getFromDB(true, hqlEntity, dbEntry);
	}

	public static MultiKey getUniqueAttributesFromDictionnary(MultiKey multiKey,
			Map<MultiKey, Map<String, Object>> dictionnary) throws DataException {

		HQLSafePathEntity<?> hqlEntity = getHQLEntityByClassname((String) multiKey.getKey(0));
		Set<HQLSafePath<?>> uniqueAttributes = HQLEntityInfo.getUniqueAttributesSetWithoutIds(hqlEntity);

		Object[] attributes = new Object[uniqueAttributes.size() + 1];
		attributes[0] = hqlEntity.getEntityClass().getCanonicalName();

		Map<String, Object> values = dictionnary.get(multiKey);
		int i = 1;
		for (HQLSafePath<?> uniqueAttribute : uniqueAttributes) {
			String propertyName = DataPropertyUtils.getPropertyName(uniqueAttribute);
			attributes[i] = values.get(propertyName);
			if (attributes[i] instanceof MultiKey) {
				attributes[i] = getUniqueAttributesFromDictionnary((MultiKey) attributes[i], dictionnary);
			}
			i++;
		}

		return new MultiKey(attributes);
	}

	public static HQLSafePathEntity<?> getHQLEntity(Object element) throws DataException {
		String canonicalName = element.getClass().getCanonicalName();
		return getHQLEntityByClassname(canonicalName);
	}

	public static HQLSafePathEntity<?> getHQLEntityByMultikey(MultiKey element) throws DataException {
		return getHQLEntityByClassname((String) element.getKey(0));
	}

	protected static HQLSafePathEntity<?> getHQLEntityByClassname(String canonicalName) throws DataException {
		String entityName = canonicalName + "Query";
		Class<HQLSafePathEntity<?>> safePathEntityClass;
		try {
			safePathEntityClass = (Class<HQLSafePathEntity<?>>) DataExporter.class.forName(entityName);
		} catch (ClassNotFoundException e1) {
			try {
				Class<?> clazz = Class.forName(canonicalName);
				Class<?> superclass = clazz.getSuperclass();
				if (superclass == null) {
					throw new DataException("Failed to get Query...");
				}
				return getHQLEntityByClassname(superclass.getCanonicalName());
			} catch (ClassNotFoundException e) {
				throw new DataException(e);
			}
		}

		HQLSafePathEntity<?> safePathEntity;
		try {
			safePathEntity = safePathEntityClass.newInstance();
		} catch (Exception e) {
			throw new DataException("Failed to init", e);
		}
		return safePathEntity;
	}

}
