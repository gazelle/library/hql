package net.ihe.gazelle.hql.exchange;

import java.util.Collection;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.persistence.EntityManager;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.collections.keyvalue.MultiKey;

import net.ihe.gazelle.hql.exchange.common.DataException;
import net.ihe.gazelle.hql.providers.EntityManagerService;
import net.ihe.gazelle.hql.providers.detached.PerformHibernateAction;

public class DataImporterCreateCollections implements PerformHibernateAction {

	private MultiKey itemKey;
	private boolean objectLoaded = false;
	private Object object;
	private Map<MultiKey, Map<String, Object>> dictionnary;

	@Override
	public Object performAction(EntityManager entityManager, Object... context) throws Exception {
		boolean modified = false;
		this.itemKey = (MultiKey) context[0];
		this.dictionnary = (Map<MultiKey, Map<String, Object>>) context[1];

		Set<Entry<String, Object>> entrySet = dictionnary.get(itemKey).entrySet();
		for (Entry<String, Object> entry : entrySet) {
			Object value = entry.getValue();
			if (value instanceof Collection) {
				value = DataImporter.convertCollection((Collection) value, dictionnary);
				try {
					PropertyUtils.setProperty(getObject(), entry.getKey(), value);
					modified = true;
				} catch (Exception e) {
					throw new DataException(e);
				}
			}
		}

		if (modified) {
			EntityManagerService.provideEntityManager().merge(object);
		}

		return modified;
	}

	private Object getObject() throws DataException {
		if (!objectLoaded) {
			object = DataImporter.findElementByUniqueAttributes(itemKey, dictionnary);
		}
		return object;
	}
}
