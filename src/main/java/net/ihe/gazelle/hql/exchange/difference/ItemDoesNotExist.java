package net.ihe.gazelle.hql.exchange.difference;

import net.ihe.gazelle.hql.exchange.DataImporter;

import org.apache.commons.collections.keyvalue.MultiKey;

public class ItemDoesNotExist extends ExchangeDifference {

	private MultiKey itemKey;

	public ItemDoesNotExist(DataImporter dataImporter, MultiKey itemKey) {
		super(dataImporter);
		this.itemKey = itemKey;
	}

	public MultiKey getItemKey() {
		return itemKey;
	}

	public void setItemKey(MultiKey itemKey) {
		this.itemKey = itemKey;
	}

	@Override
	public String toString() {
		return "ItemDoesNotExist [itemKey=" + itemKey + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((itemKey == null) ? 0 : itemKey.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemDoesNotExist other = (ItemDoesNotExist) obj;
		if (itemKey == null) {
			if (other.itemKey != null)
				return false;
		} else if (!itemKey.equals(other.itemKey))
			return false;
		return true;
	}

}
