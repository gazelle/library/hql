package net.ihe.gazelle.hql.exchange.visitor;

import net.ihe.gazelle.hql.exchange.ExchangeSet;
import net.ihe.gazelle.hql.exchange.annotations.Exchanged;
import net.ihe.gazelle.hql.exchange.common.DataException;
import net.ihe.gazelle.hql.paths.HQLEntityInfo;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DataGraphState {

	private static Logger log = LoggerFactory.getLogger(DataGraphState.class);

	private HQLSafePathEntity rootPath;

	private List<HQLSafePathEntity> elements;

	private Class<? extends ExchangeSet> set;

	public DataGraphState(HQLSafePathEntity rootPath, Class<? extends ExchangeSet> set) {
		super();
		elements = new ArrayList<HQLSafePathEntity>();
		this.rootPath = rootPath;
		this.set = set;
		pushEntity(rootPath);
	}

	public HQLSafePathEntity getParent() {
		if (elements.size() > 1) {
			return elements.get(elements.size() - 2);
		} else {
			return null;
		}
	}

	public HQLSafePathEntity getLast() {
		if (elements.size() > 0) {
			return elements.get(elements.size() - 1);
		} else {
			return null;
		}
	}

	public void pushEntity(HQLSafePathEntity hqlSafePathEntity) {
		elements.add(hqlSafePathEntity);
	}

	public void popEntity() {
		if (elements.size() > 0) {
			elements.remove(elements.size() - 1);
		}
	}

	public Acceptance accept() throws DataException {
		HQLSafePathEntity entity = getLast();

		Class<?> entityClass = entity.getEntityClass();
		Exchanged dbSynchronized = entityClass.getAnnotation(Exchanged.class);

		if (dbSynchronized == null || !dbSynchronized.set().isAssignableFrom(set)) {
			return new Acceptance(false, "no @DBSynchronized");
		}

		Set<String> classes = new HashSet<String>();
		for (HQLSafePathEntity element : elements) {
			classes.add(element.getEntityClass().getCanonicalName());
		}
		if (classes.size() == elements.size()) {
			return new Acceptance(true, null);
		} else {
			HQLSafePathEntity parent = getParent();
			if (parent != null) {
				Set uniqueAttributesSet = HQLEntityInfo.getUniqueAttributesSet(parent);
				if (uniqueAttributesSet.contains(entity)) {
					return new Acceptance(true, null);
				}
			}

			return new Acceptance(false, "already exported");
		}
	}

}
