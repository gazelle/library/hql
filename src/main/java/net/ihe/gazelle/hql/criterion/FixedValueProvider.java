package net.ihe.gazelle.hql.criterion;

public class FixedValueProvider implements ValueProvider {

	private Object value;

	public FixedValueProvider(Object value) {
		super();
		if (value instanceof ValueProvider) {
			throw new IllegalArgumentException("Can't create a FixedValueProvider with a ValueProvider!");
		}
		this.value = value;
	}

	@Override
	public Object getValue() {
		return value;
	}

}
