package net.ihe.gazelle.hql.criterion;

import java.io.Serializable;

public abstract class IntervalValue<V> implements Serializable {

	protected IntervalType intervalType;

	public IntervalValue() {
		super();
		intervalType = IntervalType.ANY;
	}

	public IntervalType getIntervalType() {
		return intervalType;
	}

	public void setIntervalType(IntervalType intervalType) {
		this.intervalType = intervalType;
	}

	public boolean isFirstValue() {
		return intervalType != IntervalType.ANY;
	}

	public boolean isDualValue() {
		return intervalType == IntervalType.BETWEEN;
	}

	public abstract V getValue1();

	public abstract void setValue1(V value);

	public abstract V getValue2();

	public abstract void setValue2(V value);

}
