package net.ihe.gazelle.hql.criterion;

import java.io.Serializable;

import net.ihe.gazelle.hql.HQLQueryBuilder;

public interface HQLCriterion<E, F> extends Serializable {

	String getKeyword();

	Class<F> getSelectableClass();

	ValueProvider getValueFixer();

	String getSelectableLabel(F instance);

	ValueProvider getValueInitiator();

	String getPath();

	void appendDefaultFilter(HQLQueryBuilder<E> queryBuilder);

	void filter(HQLQueryBuilder<E> queryBuilder, Object filterValue);

	boolean hasPossibleValues();

	boolean isPossibleFiltered();

	boolean isCountEnabled();

	void setPossibleFiltered(boolean possibleFiltered);

	void setCountEnabled(boolean countEnabled);

}
