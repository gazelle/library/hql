package net.ihe.gazelle.hql.criterion;

import java.io.Serializable;
import java.util.Map;

import net.ihe.gazelle.hql.HQLQueryBuilder;

public interface QueryModifier<T> extends Serializable {

	void modifyQuery(HQLQueryBuilder<T> queryBuilder, Map<String, Object> filterValuesApplied);

}
