package net.ihe.gazelle.hql.criterion;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.restrictions.HQLRestrictions;

public abstract class IntervalCriterion<E, V> implements HQLCriterion<E, IntervalValue<V>> {

	private String keyword;

	private String path;

	public IntervalCriterion(String keyword, String path) {
		super();
		this.keyword = keyword;
		this.path = path;
	}

	public abstract V getRealValue1(IntervalValue<V> intervalValue);

	public abstract V getRealValue2(IntervalValue<V> intervalValue);

	protected abstract IntervalValue<V> getInitValue();

	@Override
	public boolean hasPossibleValues() {
		return false;
	}

	@Override
	public boolean isPossibleFiltered() {
		return false;
	}

	@Override
	public boolean isCountEnabled() {
		return false;
	}

	@Override
	public void setPossibleFiltered(boolean possibleFiltered) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setCountEnabled(boolean countEnabled) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getKeyword() {
		return keyword;
	}

	@Override
	public ValueProvider getValueFixer() {
		return null;
	}

	@Override
	public String getSelectableLabel(IntervalValue<V> instance) {
		return null;
	}

	@Override
	public String getPath() {
		return path;
	}

	@Override
	public void appendDefaultFilter(HQLQueryBuilder<E> queryBuilder) {
		//
	}

	@Override
	public ValueProvider getValueInitiator() {
		return new ValueProvider() {
			@Override
			public Object getValue() {
				return getInitValue();
			}
		};
	}

	@Override
	public void filter(HQLQueryBuilder<E> queryBuilder, Object filterValue) {
		if (filterValue != null && filterValue instanceof IntervalValue) {
			IntervalValue<V> intervalValue = (IntervalValue<V>) filterValue;
			Object value1 = getRealValue1(intervalValue);
			Object value2 = getRealValue2(intervalValue);
			switch (intervalValue.getIntervalType()) {
			case BEFORE:
				queryBuilder.addRestriction(HQLRestrictions.le(path, value1));
				break;
			case AFTER:
				queryBuilder.addRestriction(HQLRestrictions.ge(path, value1));
				break;
			case BETWEEN:
				queryBuilder.addRestriction(HQLRestrictions.ge(path, value1));
				queryBuilder.addRestriction(HQLRestrictions.le(path, value2));
				break;
			case ANY:
				break;
			default:
				break;
			}
		}
	}

}
