package net.ihe.gazelle.hql.criterion.date;

import java.io.Serializable;
import java.util.Date;
import java.util.TimeZone;

import net.ihe.gazelle.hql.criterion.IntervalType;
import net.ihe.gazelle.hql.criterion.IntervalValue;

public class DateValue extends IntervalValue<Date> {

	private static final long serialVersionUID = 647338751726611665L;

	private Date date1 = new Date();

	private Date date2 = new Date();

	private TimeZone userTimeZone;

	public DateValue(TimeZone userTimeZone) {
		super();
		date1 = new Date();
		date2 = new Date();
		this.userTimeZone = userTimeZone;
	}

	@Override
	public void setIntervalType(IntervalType intervalType) {
		super.setIntervalType(intervalType);
		if (intervalType == IntervalType.BETWEEN) {
			if (date1.after(date2)) {
				date2 = DateCriterion.nextDay(date1, userTimeZone);
			}
		}
	}

	@Override
	public Date getValue1() {
		return date1;
	}

	@Override
	public void setValue1(Date value) {
		this.date1 = value;
		if (intervalType == IntervalType.BETWEEN) {
			if (date1.after(date2)) {
				date2 = DateCriterion.getDayEnd(date1, userTimeZone);
			}
		}
	}

	@Override
	public Date getValue2() {
		return date2;
	}

	@Override
	public void setValue2(Date value) {
		this.date2 = value;
		if (intervalType == IntervalType.BETWEEN) {
			if (date2.before(date1)) {
				date1 = DateCriterion.getDayStart(date2, userTimeZone);
			}
		}
	}

}
