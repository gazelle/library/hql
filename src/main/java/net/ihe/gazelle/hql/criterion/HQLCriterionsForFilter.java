package net.ihe.gazelle.hql.criterion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.ihe.gazelle.hql.paths.HQLSafePath;
import net.ihe.gazelle.hql.paths.HQLSafePathEntity;

public class HQLCriterionsForFilter<T> {

	private HQLSafePathEntity<T> hqlEntity;

	private List<HQLCriterion<T, ?>> criterions;

	private List<QueryModifier<T>> queryModifiers;

	private Map<String, List<QueryModifier<T>>> queryModifiersForSuggest;

	public HQLCriterionsForFilter(HQLSafePathEntity<T> hqlEntity) {
		this(hqlEntity, null);
	}

	public HQLCriterionsForFilter(HQLSafePathEntity<T> hqlEntity, QueryModifier<T> queryModifier) {
		super();
		this.hqlEntity = hqlEntity;
		this.queryModifiers = new ArrayList<QueryModifier<T>>(1);
		if (queryModifier != null) {
			this.queryModifiers.add(queryModifier);
		}
		this.criterions = new ArrayList<HQLCriterion<T, ?>>();
		this.queryModifiersForSuggest = new HashMap<String, List<QueryModifier<T>>>();
	}

	public HQLCriterion addPath(String keyword, HQLSafePath path) {
		return addPath(keyword, path, null, null);
	}

	public HQLCriterion addPath(String keyword, HQLSafePath path, Object defaultValue) {
		return addPath(keyword, path, defaultValue, null);
	}

	public HQLCriterion addPath(String keyword, HQLSafePath path, ValueProvider defaultValue) {
		return addPath(keyword, path, defaultValue, null);
	}

	public HQLCriterion addPath(String keyword, HQLSafePath path, Object defaultValue, Object fixedValue) {
		return addPath(keyword, path, new FixedValueProvider(defaultValue), new FixedValueProvider(fixedValue));
	}

	public HQLCriterion addPath(String keyword, HQLSafePath path, ValueProvider defaultValue, Object fixedValue) {
		return addPath(keyword, path, defaultValue, new FixedValueProvider(fixedValue));
	}

	public HQLCriterion addPath(String keyword, HQLSafePath path, Object defaultValue, ValueProvider fixedValue) {
		return addPath(keyword, path, new FixedValueProvider(defaultValue), fixedValue);
	}

	public HQLCriterion addPath(String keyword, HQLSafePath path, ValueProvider defaultValue, ValueProvider fixedValue) {
		HQLCriterion criterion = path.getCriterion(keyword);
		if (criterion instanceof AbstractCriterion) {
			AbstractCriterion abstractCriterion = (AbstractCriterion) criterion;
			if (defaultValue != null) {
				abstractCriterion.setValueInitiator(defaultValue);
			}
			if (fixedValue != null) {
				abstractCriterion.setValueFixer(fixedValue);
			}
		}
		criterions.add(criterion);
		return criterion;
	}

	public Class<T> getEntityClass() {
		return (Class<T>) hqlEntity.getEntityClass();
	}

	public void addQueryModifier(QueryModifier<T> queryModifier) {
		queryModifiers.add(queryModifier);
	}

	public void addQueryModifierForCriterion(String keyword, QueryModifier<T> queryModifier) {
		List<QueryModifier<T>> queryModifiers = queryModifiersForSuggest.get(keyword);
		if (queryModifiers == null) {
			queryModifiers = new ArrayList<QueryModifier<T>>();
			queryModifiersForSuggest.put(keyword, queryModifiers);
		}
		queryModifiers.add(queryModifier);
	}

	public List<QueryModifier<T>> getQueryModifiers() {
		return queryModifiers;
	}

	public Map<String, List<QueryModifier<T>>> getQueryModifiersForSuggest() {
		return queryModifiersForSuggest;
	}

	public List<HQLCriterion<T, ?>> getCriterionsList() {
		return this.criterions;
	}

	public void addCriterion(HQLCriterion hqlCriterion) {
		criterions.add(hqlCriterion);
	}

}
