/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.hql.criterion;

import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.LabelProvider;
import net.ihe.gazelle.hql.NullValue;

public abstract class AbstractCriterion<E, F> implements HQLCriterion<E, F> {

	protected Class<F> selectableClass;
	private String keyword;
	private ValueProvider valueFixer;
	private ValueProvider valueInitiator;
	private LabelProvider<F> labelProvider;
	private boolean possibleFiltered = true;
	private boolean countEnabled = true;

	public AbstractCriterion(Class<F> selectableClass, String keyword) {
		super();
		this.selectableClass = selectableClass;
		this.keyword = keyword;
	}

	@Override
	public boolean hasPossibleValues() {
		return true;
	}

	@Override
	public boolean isPossibleFiltered() {
		return possibleFiltered;
	}

	@Override
	public boolean isCountEnabled() {
		return countEnabled;
	}

	@Override
	public void setPossibleFiltered(boolean possibleFiltered) {
		this.possibleFiltered = possibleFiltered;
	}

	@Override
	public void setCountEnabled(boolean countEnabled) {
		this.countEnabled = countEnabled;
	}

	public Class<F> getSelectableClass() {
		return selectableClass;
	}

	public String getKeyword() {
		return keyword;
	}

	public void filter(HQLQueryBuilder<E> queryBuilder, Object filterValue) {
		String path = getPath();
		queryBuilder.addEq(path, filterValue);
	}

	public void appendDefaultFilter(HQLQueryBuilder<E> queryBuilder) {
		// None by default
	}

	public ValueProvider getValueFixer() {
		return valueFixer;
	}

	public void setValueFixer(ValueProvider valueFixer) {
		this.valueFixer = valueFixer;
	}

	public ValueProvider getValueInitiator() {
		return valueInitiator;
	}

	public LabelProvider<F> getLabelProvider() {
		return labelProvider;
	}

	public void setValueInitiator(ValueProvider valueInitiator) {
		this.valueInitiator = valueInitiator;
	}

	public void setLabelProvider(LabelProvider<F> labelProvider) {
		this.labelProvider = labelProvider;
	}

	public final String getSelectableLabel(F instance) {
		if (NullValue.NULL_VALUE.equals(instance) || instance == null) {
			return "Not defined";
		} else {
			if (labelProvider != null) {
				return labelProvider.getLabel(instance);
			} else {
				return getSelectableLabelNotNull(instance);
			}
		}
	}

	public abstract String getSelectableLabelNotNull(F instance);

	public abstract String getPath();

}
