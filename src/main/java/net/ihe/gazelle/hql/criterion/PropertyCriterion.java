/*******************************************************************************
 * Copyright 2011 IHE International (http://www.ihe.net)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *******************************************************************************/
package net.ihe.gazelle.hql.criterion;

import java.util.List;

import net.ihe.gazelle.hql.FilterLabelProvider;
import net.ihe.gazelle.hql.HQLQueryBuilder;

import org.apache.commons.lang.StringUtils;

public class PropertyCriterion<E, F> extends AbstractCriterion<E, F> {

	protected String associationPath;
	protected String propertyName;
	private Class<?> realSelectableClass;

	public PropertyCriterion(Class<F> selectableClass, String keyword, String associationPath, String propertyName) {
		super(selectableClass, keyword);
		this.associationPath = associationPath;
		this.propertyName = propertyName;
		this.realSelectableClass = selectableClass;
	}

	public PropertyCriterion(Class<F> selectableClass, String keyword, String path) {
		super(selectableClass, keyword);

		int lastIndexOf = path.lastIndexOf('.');
		if (lastIndexOf == -1) {
			associationPath = "this";
			propertyName = path;
		} else {
			associationPath = path.substring(0, lastIndexOf);
			propertyName = path.substring(lastIndexOf + 1);
		}

		this.realSelectableClass = selectableClass;
	}

	public String getPath() {
		String path;
		if (StringUtils.trimToNull(associationPath) == null || associationPath.equals("this")) {
			path = propertyName;
		} else {
			path = associationPath + "." + propertyName;
		}
		return path;
	}

	/**
	 * Override this method if you want to see the info link for the linked
	 * item.
	 * 
	 * @return
	 */
	public Class<?> getRealSelectableClass() {
		return realSelectableClass;
	}

	public void setRealSelectableClass(Class<?> realSelectableClass) {
		this.realSelectableClass = realSelectableClass;
	}

	@Override
	public String getSelectableLabelNotNull(F instance) {
		if (instance instanceof FilterLabelProvider) {
			return ((FilterLabelProvider) instance).getSelectableLabel();
		} else {
			return instance.toString();
		}
	}

	public Object getRealValue(F filterValue) {
		HQLQueryBuilder<?> builder = new HQLQueryBuilder(getRealSelectableClass());
		builder.setMaxResults(2);
		builder.addEq(propertyName, filterValue);
		List<?> list = builder.getList();
		if (list.size() == 1) {
			return list.get(0);
		} else {
			return null;
		}
	}

}
