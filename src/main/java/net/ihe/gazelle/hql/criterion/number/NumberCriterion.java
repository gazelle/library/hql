package net.ihe.gazelle.hql.criterion.number;

import net.ihe.gazelle.hql.criterion.IntervalCriterion;
import net.ihe.gazelle.hql.criterion.IntervalValue;

public class NumberCriterion<E> extends IntervalCriterion<E, Integer> {

	private Integer minValue;
	private Integer maxValue;

	public NumberCriterion(String keyword, String path, Integer minValue, Integer maxValue) {
		super(keyword, path);
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Class<IntervalValue<Integer>> getSelectableClass() {
		Class result = NumberValue.class;
		return result;
	}

	@Override
	public Integer getRealValue1(IntervalValue<Integer> intervalValue) {
		return intervalValue.getValue1();
	}

	@Override
	public Integer getRealValue2(IntervalValue<Integer> intervalValue) {
		return intervalValue.getValue2();
	}

	@Override
	protected NumberValue getInitValue() {
		return new NumberValue(minValue, maxValue);
	}

}
