package net.ihe.gazelle.hql.criterion.date;

import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import net.ihe.gazelle.hql.criterion.IntervalCriterion;
import net.ihe.gazelle.hql.criterion.IntervalType;
import net.ihe.gazelle.hql.criterion.IntervalValue;

public class DateCriterion<E> extends IntervalCriterion<E, Date> {

	private static final long serialVersionUID = -4429548288169589563L;

	private boolean useTime;

	private TimeZone userTimeZone;

	public DateCriterion(String keyword, String path, boolean useTime, TimeZone userTimeZone) {
		super(keyword, path);
		this.useTime = useTime;
		this.userTimeZone = userTimeZone;
	}

	@Override
	public Date getRealValue1(IntervalValue<Date> intervalValue) {
		if (intervalValue.getIntervalType() == IntervalType.BEFORE) {
			return getRealEnd(intervalValue.getValue1());
		} else {
			return getRealStart(intervalValue.getValue1());
		}
	}

	@Override
	public Date getRealValue2(IntervalValue<Date> intervalValue) {
		return getRealEnd(intervalValue.getValue2());
	}

	@Override
	protected DateValue getInitValue() {
		return new DateValue(userTimeZone);
	}

	private Date getRealStart(Date date) {
		if (useTime) {
			return date;
		}
		return getDayStart(date, userTimeZone);
	}

	private Date getRealEnd(Date date) {
		if (useTime) {
			return date;
		}
		return getDayEnd(date, userTimeZone);
	}

	public boolean isUseTime() {
		return useTime;
	}

	public void setUseTime(boolean useTime) {
		this.useTime = useTime;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Class<IntervalValue<Date>> getSelectableClass() {
		Class result = DateValue.class;
		return result;
	}

	private static Calendar getCalendarSafe(TimeZone userTimeZone) {
		if (userTimeZone == null) {
			return Calendar.getInstance();
		} else {
			return Calendar.getInstance(userTimeZone);
		}
	}

	public static Date getDayStart(Date date, TimeZone userTimeZone) {
		Calendar calendar = getCalendarSafe(userTimeZone);
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}

	public static Date getDayEnd(Date date, TimeZone userTimeZone) {
		Calendar calendar = getCalendarSafe(userTimeZone);
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
		return calendar.getTime();
	}

	public static long addDays(long time, int amount, TimeZone userTimeZone) {
		Calendar calendar = getCalendarSafe(userTimeZone);
		calendar.setTimeInMillis(time);
		calendar.add(Calendar.DAY_OF_MONTH, amount);
		return calendar.getTimeInMillis();
	}

	public static Date nextDay(Date date, TimeZone userTimeZone) {
		return new Date(addDays(date.getTime(), 1, userTimeZone));
	}

	public static Date previousDay(Date date, TimeZone userTimeZone) {
		return new Date(addDays(date.getTime(), -1, userTimeZone));
	}

}
