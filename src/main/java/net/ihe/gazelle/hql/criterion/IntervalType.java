package net.ihe.gazelle.hql.criterion;

public enum IntervalType {

	ANY,

	BEFORE,

	AFTER,

	BETWEEN;

}
