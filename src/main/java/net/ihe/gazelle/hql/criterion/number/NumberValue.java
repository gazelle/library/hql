package net.ihe.gazelle.hql.criterion.number;

import net.ihe.gazelle.hql.criterion.IntervalValue;

public class NumberValue extends IntervalValue<Integer> {

	private Integer value1;

	private Integer value2;

	private Integer minValue;

	private Integer maxValue;

	public NumberValue(Integer minValue, Integer maxValue) {
		super();
		this.minValue = minValue;
		this.maxValue = maxValue;
		value1 = minValue;
		value2 = maxValue;
	}

	@Override
	public Integer getValue1() {
		return value1;
	}

	@Override
	public void setValue1(Integer value) {
		value1 = value;
	}

	public void setValue1(String value) {
		setValue1(Integer.parseInt(value));
	}

	@Override
	public Integer getValue2() {
		return value2;
	}

	@Override
	public void setValue2(Integer value) {
		value2 = value;
	}

	public void setValue2(String value) {
		setValue2(Integer.parseInt(value));
	}

	public Integer getMinValue() {
		return minValue;
	}

	public Integer getMaxValue() {
		return maxValue;
	}

	public void setMinValue(Integer minValue) {
		this.minValue = minValue;
	}

	public void setMaxValue(Integer maxValue) {
		this.maxValue = maxValue;
	}

}
