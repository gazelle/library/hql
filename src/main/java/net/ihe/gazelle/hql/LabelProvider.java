package net.ihe.gazelle.hql;

public interface LabelProvider<F> {

	String getLabel(F instance);

}
