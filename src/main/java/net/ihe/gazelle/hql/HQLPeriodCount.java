package net.ihe.gazelle.hql;

public class HQLPeriodCount {

	private int count;

	private int periodIndex;

	public HQLPeriodCount(int count, int periodIndex) {
		super();
		this.count = count;
		this.periodIndex = periodIndex;
	}

	public int getCount() {
		return count;
	}

	public int getPeriodIndex() {
		return periodIndex;
	}

	@Override
	public String toString() {
		return "HQLPeriodCount [count=" + count + ", periodIndex=" + periodIndex + "]";
	}

}
