package net.ihe.gazelle.hql.paths;

import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

@Target({ METHOD })
@Retention(RUNTIME)
public @interface Path {

	boolean id() default false;

	boolean unique() default false;

	int uniqueSet() default 0;

	boolean single() default true;

	boolean notNull() default false;

	boolean mappedBy() default false;

}
