package net.ihe.gazelle.hql.paths;

import java.lang.reflect.Method;

import net.ihe.gazelle.hql.criterion.AssociationCriterion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HQLSafePathEntityCriterion extends AssociationCriterion {

	private static final long serialVersionUID = -403684273987782428L;
	private static Logger log = LoggerFactory.getLogger(HQLSafePathEntityCriterion.class);

	public HQLSafePathEntityCriterion(Class<?> selectableClass, String keyword, String associationPath) {
		super(selectableClass, keyword, associationPath);
	}

	@Override
	public String getSelectableLabelNotNull(Object instance) {
		try {
			Method filterLabelMethod = HQLSafePathEntity.getFilterMethod(selectableClass);
			if (filterLabelMethod != null) {
				return (String) filterLabelMethod.invoke(instance);
			} else {
				return "Missing @FilterLabel!";
			}
		} catch (Exception e) {
			log.error("Failed to call label method", e);
			return "?";
		}
	}
}
