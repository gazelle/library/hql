package net.ihe.gazelle.hql.paths;

import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import net.ihe.gazelle.hql.HQLInterval;
import net.ihe.gazelle.hql.HQLIntervalCount;
import net.ihe.gazelle.hql.HQLPeriod;
import net.ihe.gazelle.hql.HQLPeriodCount;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.hql.criterion.date.DateCriterion;

public class HQLSafePathBasicDate<T> extends HQLSafePathBasicNumber<T> {

	private boolean criterionWithoutTime = false;

	private TimeZone timeZone = null;

	public HQLSafePathBasicDate(HQLSafePathEntity parent, String path, HQLQueryBuilder<?> queryBuilder, Class<?> type) {
		super(parent, path, queryBuilder, type);
	}

	public List<HQLIntervalCount> getCountPerInterval(HQLInterval interval) {
		return queryBuilder.getCountPerInterval(interval, path);
	}

	public List<HQLIntervalCount> getCountPerInterval(HQLInterval interval, Date startDate, Date endDate) {
		return queryBuilder.getCountPerInterval(interval, path, startDate, endDate);
	}

	public List<HQLPeriodCount> getCountPerPeriod(HQLPeriod period) {
		return queryBuilder.getCountPerPeriod(period, path);
	}

	public List<HQLPeriodCount> getCountPerPeriod(HQLPeriod period, Date startDate, Date endDate) {
		return queryBuilder.getCountPerPeriod(period, path, startDate, endDate);
	}

	public void setCriterionWithoutTime(TimeZone timeZone) {
		this.criterionWithoutTime = true;
		this.timeZone = timeZone;
	}

	public void setCriterionWithTime(TimeZone timeZone) {
		this.criterionWithoutTime = false;
		this.timeZone = timeZone;
	}

	@Override
	public HQLCriterion getCriterion(String keyword) {
		DateCriterion dateCriterion;
		if (criterionWithoutTime) {
			dateCriterion = new DateCriterion(keyword, path, false, timeZone);
		} else {
			dateCriterion = new DateCriterion(keyword, path, true, timeZone);
		}
		return dateCriterion;
	}

	public HQLCriterion getCriterionWithoutTime(String keyword, TimeZone userTimeZone) {
		DateCriterion dateCriterion = new DateCriterion(keyword, path, false, userTimeZone);
		return dateCriterion;
	}

}
