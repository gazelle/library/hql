package net.ihe.gazelle.hql.paths;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.ihe.gazelle.hql.exchange.ExchangeSet;
import net.ihe.gazelle.hql.exchange.common.DataException;
import net.ihe.gazelle.hql.exchange.visitor.Acceptance;
import net.ihe.gazelle.hql.exchange.visitor.DataGraphState;
import net.ihe.gazelle.hql.exchange.visitor.DataGraphVisitor;

public class HQLEntityInfo {

	public interface HQLPathTypeCriterion {

		boolean match(Path path);

	}

	private static Map<Class, List<Set<Method>>> UNIQUE_ATTRIBUTES = Collections
			.synchronizedMap(new HashMap<Class, List<Set<Method>>>());

	private static Map<Class, Set<Method>> ALL_UNIQUE_ATTRIBUTES = Collections
			.synchronizedMap(new HashMap<Class, Set<Method>>());

	private static Map<Class, Set<Method>> ID_ATTRIBUTES = Collections
			.synchronizedMap(new HashMap<Class, Set<Method>>());

	private static Map<Class, Set<Method>> MAPPEDBY_ATTRIBUTES = Collections
			.synchronizedMap(new HashMap<Class, Set<Method>>());

	private static Map<Class, Set<Method>> NOTNULL_ATTRIBUTES = Collections
			.synchronizedMap(new HashMap<Class, Set<Method>>());

	private static Map<Class, List<Method>> METHODS = Collections.synchronizedMap(new HashMap<Class, List<Method>>());

	private HQLEntityInfo() {
		super();
	}

	public static Set<HQLSafePath<?>> getAllAttributes(HQLSafePathEntity entity) throws DataException {
		List<Method> methods = getMethods(entity);

		Set<HQLSafePath<?>> result = new HashSet<HQLSafePath<?>>();

		for (Method method : methods) {
			result.add(invoke(entity, method));
		}

		return result;
	}

	public static Set<HQLSafePath<?>> getIdAttributes(HQLSafePathEntity safePathEntity) throws DataException {
		return getGenericAttributes(new HQLPathTypeCriterion() {
			@Override
			public boolean match(Path path) {
				return path.id();
			}
		}, ID_ATTRIBUTES, safePathEntity);
	}

	public static Set<HQLSafePath<?>> getMappedByAttributes(HQLSafePathEntity safePathEntity) throws DataException {
		return getGenericAttributes(new HQLPathTypeCriterion() {
			@Override
			public boolean match(Path path) {
				return path.mappedBy();
			}
		}, MAPPEDBY_ATTRIBUTES, safePathEntity);
	}

	public static Set<HQLSafePath<?>> getNotNullAttributes(HQLSafePathEntity safePathEntity) throws DataException {
		return getGenericAttributes(new HQLPathTypeCriterion() {
			@Override
			public boolean match(Path path) {
				return path.notNull();
			}
		}, NOTNULL_ATTRIBUTES, safePathEntity);
	}

	public static List<Set<HQLSafePath<?>>> getUniqueAttributes(HQLSafePathEntity safePathEntity) throws DataException {
		List<Set<Method>> uniqueAttributes = UNIQUE_ATTRIBUTES.get(safePathEntity.getEntityClass());
		if (uniqueAttributes == null) {
			synchronized (HQLEntityInfo.class) {
				uniqueAttributes = UNIQUE_ATTRIBUTES.get(safePathEntity.getEntityClass());
				if (uniqueAttributes == null) {
					uniqueAttributes = new ArrayList<Set<Method>>();

					List<Method> methods = getMethods(safePathEntity);
					for (Method method : methods) {
						Path path = method.getAnnotation(Path.class);
						boolean unique = path.unique();
						int uniqueSet = path.uniqueSet();
						while (uniqueAttributes.size() <= uniqueSet) {
							uniqueAttributes.add(new HashSet<Method>());
						}
						uniqueAttributes.get(uniqueSet).add(method);
					}

					UNIQUE_ATTRIBUTES.put(safePathEntity.getEntityClass(), uniqueAttributes);
				}
			}
		}

		List<Set<HQLSafePath<?>>> result;
		result = new ArrayList<Set<HQLSafePath<?>>>();

		for (Set<Method> set : uniqueAttributes) {
			Set<HQLSafePath<?>> attributes = new HashSet<HQLSafePath<?>>();
			for (Method method : set) {
				attributes.add(invoke(safePathEntity, method));
			}
			result.add(attributes);
		}

		return result;
	}

	public static Set<HQLSafePath<?>> getUniqueAttributesSet(HQLSafePathEntity safePathEntity) throws DataException {
		return getGenericAttributes(new HQLPathTypeCriterion() {
			@Override
			public boolean match(Path path) {
				return path.unique();
			}
		}, ALL_UNIQUE_ATTRIBUTES, safePathEntity);
	}

	public static Set<HQLSafePath<?>> getGenericAttributes(HQLPathTypeCriterion criterion,
			Map<Class, Set<Method>> constant, HQLSafePathEntity safePathEntity) throws DataException {
		Set<Method> matchingAttributes = constant.get(safePathEntity.getEntityClass());
		if (matchingAttributes == null) {
			synchronized (HQLEntityInfo.class) {
				matchingAttributes = constant.get(safePathEntity.getEntityClass());
				if (matchingAttributes == null) {
					matchingAttributes = new HashSet<Method>();

					List<Method> methods = getMethods(safePathEntity);
					for (Method method : methods) {
						Path path = method.getAnnotation(Path.class);
						if (criterion.match(path)) {
							matchingAttributes.add(method);
						}
					}
					constant.put(safePathEntity.getEntityClass(), matchingAttributes);
				}
			}
		}

		Set<HQLSafePath<?>> result = new HashSet<HQLSafePath<?>>();

		for (Method method : matchingAttributes) {
			result.add(invoke(safePathEntity, method));
		}

		return result;
	}

	private static List<Method> getMethods(HQLSafePathEntity safePathEntity) {
		List<Method> methods = METHODS.get(safePathEntity.getEntityClass());
		if (methods == null) {
			synchronized (HQLEntityInfo.class) {
				methods = METHODS.get(safePathEntity.getEntityClass());
				if (methods == null) {
					methods = new ArrayList<Method>();

					addMethods(methods, safePathEntity.getClass());

					METHODS.put(safePathEntity.getEntityClass(), methods);
				}
			}
		}
		return methods;
	}

	private static void addMethods(List<Method> methods, Class<?> entityClass) {

		Method[] methodArray = entityClass.getMethods();
		for (Method method : methodArray) {
			Path pathAnnotation = method.getAnnotation(Path.class);
			if (pathAnnotation != null) {
				methods.add(method);
			}
		}
		if (entityClass.getSuperclass() != null) {
			addMethods(methods, entityClass.getSuperclass());
		}

	}

	private static HQLSafePath<?> invoke(HQLSafePathEntity safePathEntity, Method method) throws DataException {
		try {
			return (HQLSafePath<?>) method.invoke(safePathEntity);
		} catch (IllegalArgumentException e) {
			throw new DataException(e);
		} catch (IllegalAccessException e) {
			throw new DataException(e);
		} catch (InvocationTargetException e) {
			throw new DataException(e);
		}
	}

	public static Set<HQLSafePath<?>> getUniqueAttributesSetWithoutIds(HQLSafePathEntity safePathEntity)
			throws DataException {
		Set<HQLSafePath<?>> uniqueAttributesSet = getUniqueAttributesSet(safePathEntity);
		uniqueAttributesSet.removeAll(getIdAttributes(safePathEntity));
		if (uniqueAttributesSet.size() == 0) {
			throw new DataException("Uniqueness is only defined by ids on " + safePathEntity.getEntityClass() + " !");
		}
		return uniqueAttributesSet;
	}

	public static boolean isUniqueAttribute(HQLSafePathEntity parent, HQLSafePathBasic child) {
		List<Set<HQLSafePath<?>>> uniqueAttributes;
		try {
			uniqueAttributes = getUniqueAttributes(parent);
		} catch (DataException e) {
			return false;
		}

		for (Set<HQLSafePath<?>> set : uniqueAttributes) {
			if (set.size() == 1) {
				HQLSafePath<?> hqlSafePath = set.iterator().next();
				if (child.getPath().equals(hqlSafePath.getPath())) {
					return true;
				}
			}
		}
		return false;
	}

	public static void visitDataGraph(HQLSafePathEntity safePathEntity, DataGraphVisitor visitor,
			Class<? extends ExchangeSet> set) throws DataException {
		DataGraphState dataGraphState = new DataGraphState(safePathEntity, set);
		visitDataGraph(safePathEntity, dataGraphState, visitor);
	}

	private static void visitDataGraph(HQLSafePathEntity parentEntity, DataGraphState dataGraphState,
			DataGraphVisitor visitor) throws DataException {
		visitor.acceptEntity(dataGraphState.getParent(), parentEntity);
		Set<HQLSafePath<?>> allAttributes = getAllAttributes(parentEntity);
		for (HQLSafePath<?> childPath : allAttributes) {
			if (childPath instanceof HQLSafePathBasic) {
				visitor.acceptBasic(parentEntity, (HQLSafePathBasic) childPath);
			} else if (childPath instanceof HQLSafePathEntity) {
				HQLSafePathEntity childEntity = (HQLSafePathEntity) childPath;
				dataGraphState.pushEntity(childEntity);
				Acceptance acceptance = dataGraphState.accept();
				if (acceptance.isAccepted()) {
					visitDataGraph(childEntity, dataGraphState, visitor);
				} else {
					visitor.refuseEntity(parentEntity, childEntity, acceptance.getCause());
				}
				dataGraphState.popEntity();
			}
		}
	}

}
