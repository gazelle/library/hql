package net.ihe.gazelle.hql.paths;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import net.ihe.gazelle.hql.FilterLabel;
import net.ihe.gazelle.hql.HQLQueryBuilder;
import net.ihe.gazelle.hql.criterion.AssociationCriterion;
import net.ihe.gazelle.hql.criterion.HQLCriterion;
import net.ihe.gazelle.hql.criterion.HQLCriterionsForFilter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class HQLSafePathEntity<T> extends HQLSafePath<T> {

	private static Logger log = LoggerFactory.getLogger(HQLSafePathEntity.class);

	public static Map<Class<?>, Method> filterLabelMethods = Collections
			.synchronizedMap(new HashMap<Class<?>, Method>());

	public static Map<Class<?>, Boolean> filterLabelMethodInit = Collections
			.synchronizedMap(new HashMap<Class<?>, Boolean>());

	private static void initFilterLabelMethod(Class<?> entityClass) {
		synchronized (entityClass) {
			if (filterLabelMethodInit.get(entityClass) == null) {
				Method[] methods = entityClass.getMethods();
				for (Method method : methods) {
					FilterLabel annotation = method.getAnnotation(FilterLabel.class);
					if (annotation != null) {
						filterLabelMethods.put(entityClass, method);
					}
				}
				if (filterLabelMethods.get(entityClass) == null) {
					log.error("No @FilterLabel on class " + entityClass.getCanonicalName() + " !!!!!!");
				}
				filterLabelMethodInit.put(entityClass, Boolean.TRUE);
			}
		}
	}

	public static void declareFilterLabel(Class<?> entityClass, String methodName) {
		synchronized (entityClass) {
			Method[] methods = entityClass.getMethods();
			for (Method method : methods) {
				if (method.getName().equals(methodName)) {
					filterLabelMethods.put(entityClass, method);
					filterLabelMethodInit.put(entityClass, Boolean.TRUE);
				}
			}
			if (filterLabelMethods.get(entityClass) == null) {
				log.error("Failed to find " + entityClass.getCanonicalName() + "." + methodName + "() !!!!!!");
			}
		}
	}

	public static Method getFilterMethod(Class<?> entityClass) {
		if (filterLabelMethodInit.get(entityClass) == null) {
			initFilterLabelMethod(entityClass);
		}
		return filterLabelMethods.get(entityClass);
	}

	public HQLSafePathEntity(String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
	}

	public HQLCriterionsForFilter<T> getHQLCriterionsForFilter() {
		return new HQLCriterionsForFilter<T>(this);
	}

	@SuppressWarnings({ "rawtypes" })
	public HQLCriterion getCriterion(String keyword) {
		Class<?> entityClass = getEntityClass();
		AssociationCriterion associationCriterion = new HQLSafePathEntityCriterion(entityClass, keyword, path);
		return associationCriterion;
	}

	public void addFetch() {
		queryBuilder.addFetch(path);
	}

	public abstract boolean isSingle();

	public abstract Class<?> getEntityClass();

	/*
	private boolean single;

	public HQLSafePathEntity(boolean single, String path, HQLQueryBuilder<?> queryBuilder) {
		super(path, queryBuilder);
		this.single = single;
	}

	public boolean isSingle() {
		return single;
	}
	 */
}
