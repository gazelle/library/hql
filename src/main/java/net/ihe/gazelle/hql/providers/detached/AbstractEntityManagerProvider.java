package net.ihe.gazelle.hql.providers.detached;

import net.ihe.gazelle.hql.providers.EntityManagerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceUnit;

public abstract class AbstractEntityManagerProvider implements EntityManagerProvider {

	protected static final Logger log = LoggerFactory.getLogger(AbstractEntityManagerProvider.class);

	@PersistenceUnit
	private static EntityManagerFactory entityManagerFactory;

	// META-INF/hibernate.cfg.xml
	public abstract String getHibernateConfigPath();

	protected EntityManagerFactory getEntityManagerFactory() {
		if (entityManagerFactory == null) {
			synchronized (AbstractEntityManagerProvider.class) {
				if (entityManagerFactory == null) {
					entityManagerFactory = null;
					log.error("Failed to inject entity manager factory !!");
				}
			}
		}
		return entityManagerFactory;
	}

	@Override
	public EntityManager provideEntityManager() {
		EntityManager em = HibernateActionPerformer.ENTITYMANGER_THREADLOCAL.get();
		if (em != null) {
			return em;
		} else {
			em = getEntityManagerFactory().createEntityManager();
			return em;
		}
	}

	@Override
	public int compareTo(EntityManagerProvider o) {
		return getWeight().compareTo(o.getWeight());
	}

}
