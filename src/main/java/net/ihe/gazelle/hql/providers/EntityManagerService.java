package net.ihe.gazelle.hql.providers;

import net.ihe.gazelle.hql.providers.detached.AbstractEntityManagerProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.enterprise.inject.spi.CDI;
import javax.persistence.EntityManager;

public class EntityManagerService {

	private static final Logger log = LoggerFactory.getLogger(EntityManagerService.class);

	private EntityManagerService() {
		super();
	}

	public static EntityManager provideEntityManager() {

		EntityManagerProvider emp = CDI.current().select(AbstractEntityManagerProvider.class).get();
		if (emp != null){
			return emp.provideEntityManager();
		}
		log.error("Failed to find Entity Manager Provider");
		return null;
	}

}
