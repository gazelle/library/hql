package net.ihe.gazelle.hql.providers.detached;

import java.sql.BatchUpdateException;

import javax.enterprise.inject.spi.CDI;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceException;
import javax.persistence.RollbackException;

import net.ihe.gazelle.hql.providers.EntityManagerProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HibernateActionPerformer {

    private static final Logger log = LoggerFactory.getLogger(HibernateActionPerformer.class);

    public static final ThreadLocal<EntityManager> ENTITYMANGER_THREADLOCAL = new ThreadLocal<EntityManager>();

    public static Object performHibernateAction(PerformHibernateAction action, Object... context)
            throws HibernateFailure {
        boolean isJTA = false;
        //clear properly previous connections.
        transactionClose();

        EntityManager entityManager = CDI.current().select(EntityManagerProvider.class).get()
                .provideEntityManager();
        ENTITYMANGER_THREADLOCAL.set(entityManager);
        entityManager = getEntityManager();
        try {
            if (entityManager.getTransaction() != null) {
                entityManager.getTransaction().isActive();
            }
        } catch (IllegalStateException e) {
            isJTA = true;
        }

        Object result = null;

        if (isJTA) {
            try {
                result = action.performAction(entityManager, context);
            } catch (Throwable e) {
                throw new HibernateFailure("Something went wrong!", e);
            }
        } else {
            Throwable failure = null;

            transactionStart(entityManager);

            // Ceinture, bretelles
            try {
                result = action.performAction(entityManager, context);
            } catch (Throwable e) {
                failure = e;
                transactionError(e);
            }

            Throwable transactionCloseFailure = transactionClose();

            if (failure != null) {
                throw new HibernateFailure("Something went wrong!", failure);
            }
            if (transactionCloseFailure != null) {
                throw new HibernateFailure("Failed to close transaction!", transactionCloseFailure);
            }
        }

        return result;
    }

    private static EntityManager getEntityManager() {
        return ENTITYMANGER_THREADLOCAL.get();
    }

    static void transactionStart(EntityManager entityManager) {
        if (!checkActive(entityManager)) {
            entityManager.getTransaction().begin();
        }
    }

    static void transactionError(Throwable e) {
        log.error("Caught exception, marking transaction to be rollbacked", e);
        EntityManager entityManager = getEntityManager();
        if (checkActive(entityManager)) {
            entityManager.getTransaction().setRollbackOnly();
        } else {
            log.warn("Inactive transaction as marking rollback");
        }
    }

    static Throwable transactionClose() {
        Throwable failure = null;
        EntityManager entityManager = getEntityManager();
        if (entityManager != null) {
            if (entityManager.isOpen()) {
                try {
                    if (checkActive(entityManager)) {
                        if (entityManager.getTransaction().getRollbackOnly()) {
                            try {
                                entityManager.getTransaction().rollback();
                            } catch (PersistenceException e) {
                                failure = e;
                                log.error("Failed to rollback transaction", e);
                            }
                        } else {
                            try {
                                entityManager.getTransaction().commit();
                            } catch (RollbackException e) {
                                failure = e;
                                log.error("Failed to commit transaction", e);
                                searchBatchUpdateException(e);
                            }
                        }
                    } else {
                        log.debug("Inactive transaction, entityManager will be closed.");
                    }
                } catch (Throwable e) {
                    log.error("Caught exception, releasing entity manager anyway", e.getMessage());
                    failure = e;
                }

                try {
                    entityManager.close();
                } catch (IllegalStateException e) {
                    log.error("Failed to close entity manager", e.getMessage());
                    failure = e;
                }
            }
        }
        ENTITYMANGER_THREADLOCAL.remove();
        return failure;
    }

    private static void searchBatchUpdateException(Throwable throwable) {
        if (throwable instanceof BatchUpdateException) {
            BatchUpdateException batchUpdateException = (BatchUpdateException) throwable;
            log.error("Next exception", batchUpdateException.getNextException());
        } else {
            if (throwable.getCause() != null) {
                searchBatchUpdateException(throwable.getCause());
            }
        }
    }

    private static boolean checkActive(EntityManager entityManager) {
        return entityManager.getTransaction().isActive();
    }

}
