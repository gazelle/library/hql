package net.ihe.gazelle.hql;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.beanutils.PropertyUtils;
import org.hibernate.collection.internal.AbstractPersistentCollection;
import org.hibernate.engine.spi.SessionImplementor;
//import org.hibernate.collection.AbstractPersistentCollection;
//import org.hibernate.engine.SessionImplementor;
import org.hibernate.proxy.HibernateProxy;

public class HibernateHelper {

	private HibernateHelper() {
		super();
	}

	public static boolean getLazyEquals(Object o1, Object o2) {
		if (o1 == o2) {
			return true;
		}
		if (o1 == null) {
			return false;
		}
		if (o2 == null) {
			return false;
		}

		Class<?> c1 = o1.getClass();
		if (o1 instanceof HibernateProxy) {
			c1 = ((HibernateProxy) o1).getHibernateLazyInitializer().getPersistentClass();
		}
		Class<?> c2 = o2.getClass();
		if (o2 instanceof HibernateProxy) {
			c2 = ((HibernateProxy) o2).getHibernateLazyInitializer().getPersistentClass();
		}

		if (!c1.equals(c2)) {
			return false;
		}

		Object id1 = getProperty(o1, "id");
		Object id2 = getProperty(o2, "id");

		if (id1 == id2) {
			return true;
		}
		if (id1 == null) {
			return false;
		}
		if (id2 == null) {
			return false;
		}

		return id1.equals(id2);
	}

	public static int getLazyHashcode(Object o) {
		Object id = getProperty(o, "id");
		if (id != null) {
			return id.hashCode();
		} else {
			return System.identityHashCode(o);
		}
	}

	public static <T> T getLazyValue(Object object, String property, T currentValue) {
		T result = currentValue;

		if (result instanceof HibernateProxy) {
			HibernateProxy hibernateProxy = (HibernateProxy) result;
			if (hibernateProxy.getHibernateLazyInitializer().isUninitialized()) {
				if (!isConnectedToSession(hibernateProxy)) {
					result = reloadProperty(object, property, result);
				}
			}
		} else if (result instanceof AbstractPersistentCollection) {
			AbstractPersistentCollection bag = (AbstractPersistentCollection) result;
			if (!bag.wasInitialized()) {
				if (!isConnectedToSession(bag)) {
					result = reloadProperty(object, property, result);
					result = removeHibernateType(result);
				}
			}
		}

		return result;
	}

	@SuppressWarnings({ "unchecked" })
	public static <T> T reloadProperty(Object object, String property, T result) {
		Object refreshed = HQLReloader.reloadDetached(object);
		return (T) getProperty(refreshed, property);
	}

	public static Object getProperty(Object target, String property) {
		try {
			return PropertyUtils.getProperty(target, property);
		} catch (Exception e) {
			throw new IllegalArgumentException("Failed to get " + property + " on " + target, e);
		}
	}

	static boolean isConnectedToSession(HibernateProxy hibernateProxy) {
		SessionImplementor session = hibernateProxy.getHibernateLazyInitializer().getSession();
		return isSessionOpen(session) && session.getPersistenceContext().containsProxy(hibernateProxy);
	}

	static boolean isConnectedToSession(AbstractPersistentCollection bag) {
		SessionImplementor session = bag.getSession();
		return isSessionOpen(session) && session.getPersistenceContext().containsCollection(bag);
	}

	public static boolean isSessionOpen(SessionImplementor session) {
		return session != null && session.isConnected() && session.isOpen();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	static <T> T removeHibernateType(T result) {
		if (result instanceof List) {
			result = (T) new ArrayList((List<?>) result);
		} else if (result instanceof Set) {
			result = (T) new HashSet((Set<?>) result);
		}
		return result;
	}

}
