package net.ihe.gazelle.hql;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import javax.persistence.EntityManager;

@RunWith(MockitoJUnitRunner.class)
public class HQLQueryBuilderTest {

    @Mock
    EntityManager entityManager ;

    @Mock
    HQLQueryBuilderCache hqlQueryBuilderCache;

    HQLQueryBuildForTest<EntityTest> hqlQueryBuilder ;

    @Before
    public void setUp() {
        hqlQueryBuilder = new HQLQueryBuildForTest<>(entityManager, EntityTest.class);
        Mockito.when(hqlQueryBuilderCache.getAlias(Mockito.anyString())).thenAnswer(new Answer<String>() {
            @Override
            public String answer(InvocationOnMock invocation){
                Object[] args = invocation.getArguments();
                String path = (String) args[0];
                int lastIndexOf = path.lastIndexOf('.');
                String propertyName = path.substring(lastIndexOf + 1);
                String alias = propertyName + "_";
                return alias;
            }
        });
        Mockito.when(hqlQueryBuilderCache.isBagTypeOfEnum("this_.bagTypeOfEnum")).thenReturn(true);
        Mockito.when(hqlQueryBuilderCache.isBagTypeOfEnum("this_.bagTypeOfEnum.bagTypeOfEnum2")).thenReturn(true);
        Mockito.when(hqlQueryBuilderCache.isBagTypeOfEnum("this_.bagType")).thenReturn(false);
        Mockito.when(hqlQueryBuilderCache.isBagTypeOfEnum("this_.bagType.bagType2")).thenReturn(false);
    }

    @Test
    public void getShortPropertyTestLongPath() {
        String path="this.titi.toto.tutu" ;
        Assert.assertEquals("toto_.tutu", hqlQueryBuilder.getShortProperty(path));
    }

    @Test
    public void getShortPropertyTestShortPath() {
        String path="this.tutu" ;
        Assert.assertEquals("this_.tutu", hqlQueryBuilder.getShortProperty(path));
    }

    @Test
    public void getShortPropertyTestThis() {
        String path="this" ;
        Assert.assertEquals("this_", hqlQueryBuilder.getShortProperty(path));
    }


    @Test
    public void getShortPropertyTestShortPathBagType() {
        String path="this.bagTypeOfEnum" ;
        Assert.assertEquals("bagTypeOfEnum_", hqlQueryBuilder.getShortProperty(path));
    }

    @Test
    public void getShortPropertyTestShortPathBagTypeNoEnum() {
        String path="this.bagType" ;
        Assert.assertEquals("this_.bagType", hqlQueryBuilder.getShortProperty(path));
    }

    @Test
    public void buildQueryFromTestShortPathBagType(){
        String path="this.bagTypeOfEnum" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ \r\n" +
                        " left join this_.bagTypeOfEnum as bagTypeOfEnum_",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestShortPathBagTypeNoEnum(){
        String path="this.bagType" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ ",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestShortPathTwoBagType(){
        String path="this.bagTypeOfEnum.bagTypeOfEnum2" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ \r\n" +
                        " left join this_.bagTypeOfEnum as bagTypeOfEnum_\r\n" +
                        " left join bagTypeOfEnum_.bagTypeOfEnum2 as bagTypeOfEnum2_",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestShortPathTwoBagTypeNoEnum(){
        String path="this.bagType.bagType2" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ \r\n" +
                        " left join this_.bagType as bagType_",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestShortPath(){
        String path="this.tutu" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ ",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestLongPath(){
        String path="this.tutu.toto" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ \r\n" +
                        " left join this_.tutu as tutu_",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestVeryLongPath(){
        String path="this.tutu.titi.toto" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ \r\n" +
                        " left join this_.tutu as tutu_\r\n" +
                        " left join tutu_.titi as titi_",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestVeryLongPathWithBagType(){
        String path="this.bagTypeOfEnum.titi.toto" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ \r\n" +
                        " left join this_.bagTypeOfEnum as bagTypeOfEnum_\r\n" +
                        " left join bagTypeOfEnum_.titi as titi_",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestVeryLongPathWithBagTypeNoEnum(){
        String path="this.bagType.titi.toto" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ \r\n" +
                        " left join this_.bagType as bagType_\r\n" +
                        " left join bagType_.titi as titi_",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestVeryLongPathWithTwoBagTypes(){
        String path="this.bagTypeOfEnum.bagTypeOfEnum2.toto" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ \r\n" +
                        " left join this_.bagTypeOfEnum as bagTypeOfEnum_\r\n" +
                        " left join bagTypeOfEnum_.bagTypeOfEnum2 as bagTypeOfEnum2_",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    @Test
    public void buildQueryFromTestVeryLongPathWithTwoBagTypesNoEnum(){
        String path="this.bagType.bagType2.toto" ;
        hqlQueryBuilder.getShortProperty(path);
        Assert.assertEquals("\r\n from net.ihe.gazelle.hql.HQLQueryBuilderTest.EntityTest as this_ \r\n" +
                        " left join this_.bagType as bagType_\r\n" +
                        " left join bagType_.bagType2 as bagType2_",
                hqlQueryBuilder.buildQueryFrom().toString());
    }

    private class EntityTest {

    }

    private class HQLQueryBuildForTest<T> extends HQLQueryBuilder<T> {

        public HQLQueryBuildForTest(EntityManager entityManager, Class<T> entityClass) {
            super(entityManager, entityClass);
        }

        @Override
        protected HQLQueryBuilderCache getHqlQueryBuilderCacheTmp(Class<T> entityClass, EntityManager entityManagerQuery) {
            return hqlQueryBuilderCache;
        }
    }

}
