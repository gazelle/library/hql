package net.ihe.gazelle.hql;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.TestCase;

public class HQLIntervalTest extends TestCase {

	public void testAddMissingIntervals() {
		List<HQLIntervalCount>[] resultsList = new List[3];
		resultsList[0] = new ArrayList<HQLIntervalCount>();
		resultsList[1] = new ArrayList<HQLIntervalCount>();
		resultsList[2] = new ArrayList<HQLIntervalCount>();

		Date start = new Date();
		for (int i = 0; i < 30; i++) {
			if (i % 3 == 0) {
				resultsList[0].add(new HQLIntervalCount(12, start, HQLInterval.DAY.getEndInterval(start)));
			}
			if ((i + 1) % 5 == 0 || i == 29) {
				resultsList[1].add(new HQLIntervalCount(12, start, HQLInterval.DAY.getEndInterval(start)));
			}
			if ((i - 1) % 6 == 0) {
				resultsList[2].add(new HQLIntervalCount(12, start, HQLInterval.DAY.getEndInterval(start)));
			}
			start = HQLInterval.DAY.getEndInterval(start);
		}

		for (List<HQLIntervalCount> list : resultsList) {
			System.out.println("***");
			for (HQLIntervalCount hqlIntervalCount : list) {
				System.out.println(hqlIntervalCount);
			}
		}

		HQLInterval.DAY.addMissingIntervals(resultsList);

		for (List<HQLIntervalCount> list : resultsList) {
			System.out.println("***");
			for (HQLIntervalCount hqlIntervalCount : list) {
				System.out.println(hqlIntervalCount);
			}
		}

		assertEquals(30, resultsList[0].size());
		assertEquals(30, resultsList[1].size());
		assertEquals(30, resultsList[2].size());
	}

}
